import redis, re
import json
# 本地
pool= redis.ConnectionPool(host='127.0.0.1', port=6666, db=0, password="", decode_responses=True)
# 线上
#pool= redis.ConnectionPool(host='35.199.186.52', port=6666, db=0, password="Qury666!@#", decode_responses=True)
r = redis.Redis(connection_pool=pool)

# 芒果电影com_hunantv_imgo_activity_movie文件的qury
mang_movie= ['院线大片','爱情','喜剧','动作','科幻','青春','恐怖悬疑','战争','警匪','历史','歌舞','动画','其他']
# 芒果电视剧com_hunantv_imgo_activity_TV文件的qury
mang_tv=['甜蜜互宠','虐恋情深','青涩校园','仙侠玄幻','都市职场','古装','快意江湖','	偶像','悬疑推理','家长里短','芒果出品','轻松搞笑','铁血战争','其他']
# 京东手机品牌 com_jingdong_app_mall_phone 文件的qury
phone_list=['华为', '华为 (HUAWEI)', '小米', 'oppo','vivo','苹果']
# 京东平板品牌 com_jingdong_app_mall_flat 文件的qury
flat=['华为 (HUAWEI)','Apple','联想 (lenovo)','小米 (MI)','三星 (SAMSUNG)']
# 京东笔记本品牌 com_jingdong_app_mall_computer 文件的qury
computer =['华为 (HUAWEI)','惠普 (HP)','联想 (lenovo)','小米 (MI)','华硕 (ASUS)']
# 唯品会手机品牌 com_achievo_vipshop_phone 文件的qury
weipinhui_phone =['华为','小米','苹果','oppo', 'vivo']
# # 中关村在线的品牌
# zhong = ['手机','平板','笔记本']
# 排名靠前的汽车品牌
# dongche =['大众汽车','丰田汽车','本田汽车','奔驰汽车','沃尔沃汽车','宝马汽车','福特汽车', '别克汽车']
for i in computer:
    r.delete('com.jingdong.app.mall_computer:dupefilter')
    r.lpush('com.jingdong.app.mall_computer:start_urls', json.dumps({'query':i,'vers':'2021042712'}))


