# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class QuickSpiderItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # define the fields for your item here like:
    # name = scrapy.Field()
    appToken = scrapy.Field()
    query = scrapy.Field()
    country = scrapy.Field()
    vers = scrapy.Field()
    itemTitle = scrapy.Field()
    itemDetailLink = scrapy.Field()
    itemImageLink = scrapy.Field()
    itemType = scrapy.Field()
    category = scrapy.Field()
    itemRank = scrapy.Field()
    subtitle = scrapy.Field()
    imgHash = scrapy.Field()
    crawl_time = scrapy.Field()
    published_time = scrapy.Field()
    itemAdder = scrapy.Field()
    itemName = scrapy.Field()
    itemPrice = scrapy.Field()
    itemTime = scrapy.Field()
    itemOther = scrapy.Field()
    itemTimestamp = scrapy.Field()
    itemList = scrapy.Field()
    itemVersion = scrapy.Field()
    hostName = scrapy.Field()
    AliasCN = scrapy.Field()
    DiseaseTagName = scrapy.Field()
    RelateBody = scrapy.Field()
    DiseaseList = scrapy.Field()
    Symptom = scrapy.Field()
    Cause = scrapy.Field()
    XyTreated = scrapy.Field()
    appType = scrapy.Field()
    Onelabel = scrapy.Field()
    itemDuration= scrapy.Field()
    colour = scrapy.Field() #颜色
    Commenttags = scrapy.Field() #评论标签
    Comment = scrapy.Field() #评论
    Praise = scrapy.Field()  #好评度
    Twolabel= scrapy.Field()  #二级标签
    itemClass = scrapy.Field()  # itme的分类标签
    serverType = scrapy.Field()  # 是否是泛化词

