app_info_dict = {
    # 医疗
    'com.qisheng.hybrid': {'country': 'CN', 'category': 'medical'},
    'com.ddsy.songyao': {'country': 'CN', 'category': 'medical'},
    # 购物
    'com.jingdong.app.mall': {'country': 'CN', 'category': 'shopping'},
    'com.jingdong.app.mall_3C': {'country': 'CN', 'category': 'shopping'},
    'com.jingdong.app.mall_phone': {'country': 'CN', 'category': 'shopping'},
    'com.jingdong.app.mall_computer': {'country': 'CN', 'category': 'shopping'},
    'com.jingdong.app.mall_flat': {'country': 'CN', 'category': 'shopping'},
    'com.hunantv.imgo.activity': {'country': 'CN', 'category': 'shopping'},
    'com.achievo.vipshop_phone':{'country': 'CN', 'category': 'shopping'},
    'com.achievo.vipshop': {'country': 'CN', 'category': 'shopping'},
    'com.dangdang.buy2_phono': {'country': 'CN', 'category': 'shopping'},
    'com.dangdang.buy2_notebook': {'country': 'CN', 'category': 'shopping'},
    'com.dangdang.buy2_flat': {'country': 'CN', 'category': 'shopping'},
    # 电影
    'com.sankuai.movie': {'country': 'CN', 'category': 'movie'},
    'com.sankuai.movie_brand': {'country': 'CN', 'category': 'movie'},
    'com.dushemovie.sirmovie': {'country': 'CN', 'category': 'movie'},
    'com.dushemovie.sirmovie_brand': {'country': 'CN', 'category': 'movie'},
    'com.hunantv.imgo.activity_movie': {'country': 'CN', 'category': 'movie'},
    'com.hunantv.imgo.activity_tv': {'country': 'CN', 'category': 'movie'},
    'com.youku.phone_tv': {'country': 'CN', 'category': 'movie'},
    'com.youku.phone_tv1': {'country': 'CN', 'category': 'movie'},
    'com.youku.phone_movie': {'country': 'CN', 'category': 'movie'},
    'com.youku.phone_movie1': {'country': 'CN', 'category': 'movie'},
    'com.pplive.androidphone_tv': {'country': 'CN', 'category': 'movie'},
    'com.pplive.androidphone_movie': {'country': 'CN', 'category': 'movie'},
    # 科技
    'com.zol.android': {'country': 'CN', 'category': 'science_technology'},
    'com.zol.android_brand': {'country': 'CN', 'category': 'science_technology'},
    # 汽车
    'com.ss.android.auto': {'country': 'CN', 'category': 'car'},
    'com.xcar.activity': {'country': 'CN', 'category': 'car'},
    'com.autohome.speed': {'country': 'CN', 'category': 'car'},
    # 二手房
    'com.lianjia.beike': {'country': 'CN', 'category': 'house'},

    'club.autohome.com.cn': {'country': 'CN', 'category': 'car'},
}
