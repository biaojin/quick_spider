import os, sys
import re
import subprocess
from configs.AppInfo import app_info_dict

# 启动从爬虫


def slaver_run(start, end):
        try:
            for num in range(int(start), int(end) + 1):
                run_cmd2 = 'ssh qury@py-scrapy-' + str(
                    num) + ' "cd /home/qury/quick_spider/quick_spider ; sudo nohup python3 SlaverRunQuick.py > slaver-run.log 2>&1&"'
                os.system(run_cmd2)
        except Exception as e:
            print('error occurs during slaver run -- ' + str(e))


def shut_crawler():
    result = subprocess.getoutput('ps -ef | grep python3')
    param = result.split('\n')
    for i in list(app_info_dict.keys()):
        for line in param:
            if 'SlaverRunQuick.py' in line or i in line:
                pid = re.sub(' +', ' ', line.rstrip()).split(' ')[1]
                os.system(' kill -9 ' + str(pid))
def scp_task():
    for i in range(int(start), int(end) + 1):
        idx = str(i).zfill(2)
        crawler_name = "qury@" + 'py-scrapy' + "-" + idx
        # 复制data-platform
        dp_rm_cmd = 'ssh ' + crawler_name + ' " rm -rf /home/qury/quick_spider/"'
        dp_scp_cmd = 'scp -r /home/qury/quick_spider/quick_spider ' + crawler_name + ':/home/qury/'
        os.system(dp_rm_cmd)
        os.system(dp_scp_cmd)
        print(crawler_name + ' update succeed !')


if __name__ == '__main__':
        opt = sys.argv[1]
        start = 11
        end = 50

        # 指定服务器开启数量， 不指定则为默认
        if len(sys.argv) > 2:
            start = sys.argv[2]
            end = sys.argv[3]

        if opt == 'copy':
            scp_task()
        # python3 scp_spdier_run.py copy 11 20

        if opt == 'run_crawler':
            # 运行爬虫，开始爬取(长耗时任务, 默认为scrapy11~scrapy50)
            slaver_run(start, end)
        # python3 scp_spdier_run.py run_crawler 11 20

        if opt == 'shut_crawler':
            for i in range(int(start), int(end) + 1):
                idx = str(i).zfill(2)
                crawler_name = "qury@py-scrapy-" + idx
                print('------------{}----------'.format(crawler_name))
                task = 'ssh ' + crawler_name + ' " sudo python3 /home/qury/quick_spider/quick_spider/scp_spdier_run.py shut_crawler_slave"'
                os.system(task)

        if opt == 'shut_crawler_slave':
            shut_crawler()

        # python3 scp_spdier_run.py shut_crawler 11 20
