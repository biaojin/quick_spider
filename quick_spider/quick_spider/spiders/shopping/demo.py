#com.autohome.speed
import json
import uuid
import scrapy
import datetime
import random
import traceback
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Cache-Control': 'max-age=0',
            'Connection': 'keep-alive',
            'Cookie': 'select_city=110000; lianjia_uuid=bf28a216-d356-4cf5-a298-84304bfa6f88; UM_distinctid=179898c340f28-0e35a80bb680b8-5771e33-1fa400-179898c341026d; Hm_lvt_9152f8221cb6243a53c83b956842be8a=1621510340; _smt_uid=60a648c3.20f7e993; _jzqc=1; _jzqx=1.1621510341.1621510341.1.jzqsr=google%2Ecom|jzqct=/.-; _jzqckmp=1; _qzjc=1; _ga=GA1.2.2021507787.1621510349; _gid=GA1.2.1624963545.1621510349; lianjia_ssid=daa3bd86-428b-4bd4-b090-5db98bbe008a; CNZZDATA1253477573=1558243513-1621506254-https%253A%252F%252Fwww.google.com%252F%7C1621576570; CNZZDATA1254525948=1052397400-1621509445-https%253A%252F%252Fwww.google.com%252F%7C1621575356; CNZZDATA1255633284=678429524-1621509995-https%253A%252F%252Fwww.google.com%252F%7C1621575535; CNZZDATA1255604082=1781229101-1621509998-https%253A%252F%252Fwww.google.com%252F%7C1621578916; _jzqa=1.3367995128685065000.1621510341.1621510341.1621579837.2; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%22179898c4c6d158-002a1cd06d0e2c-5771e33-2073600-179898c4c6e35e%22%2C%22%24device_id%22%3A%22179898c4c6d158-002a1cd06d0e2c-5771e33-2073600-179898c4c6e35e%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_referrer%22%3A%22%22%2C%22%24latest_referrer_host%22%3A%22%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%7D%7D; Hm_lpvt_9152f8221cb6243a53c83b956842be8a=1621580282; _qzja=1.177624604.1621510342887.1621510342888.1621579836557.1621580273813.1621580282046.0.0.0.8.2; _qzjto=6.1.0; srcid=eyJ0Ijoie1wiZGF0YVwiOlwiZDM0NzgxNTIzNzQ0MWUzNjU1MzUzNGI1YmVmM2M2YjU0NDAzYTdiYzVhMmU1ZDUzZmI4NDBlOWE5ZWYxN2Q1ZTFkYWZlOTc3YzEzN2NlY2MwM2JhMDA5YmQzMjI3ZTI5YmM4ZjViMzY0MTRmYTdjNzM1MjZjNTZhZDQ0NzFjOWU5Mjg3NzU2NDcxOTkxNmQ3OWViMTE3ZjZlMGIyMjEzMmFlZDZlMDFmMDJiNjQxNTE5YWFiN2QyODUxNGMwMzY3ZmZiZWM3ODY4ZjViMjIwZDU1MGU2YTEzNTE1ZDg3NjQ0OGRmYmJlZjJiYTViOWE4ODY5OGE1NmE2OWM2MDRiMmFmNzgzNjYwOTBjYWI0ZTgyZTZhZTkzMWRlMzBkMjQyY2RjNTUxMDk3YTZkOWJhZWI1Njk5ZTI5NGJlYWFlNDg3ZmJhMzc1NDNhOTJiOTRjMDg4ZTkwMTAzMDA2NTFkZlwiLFwia2V5X2lkXCI6XCIxXCIsXCJzaWduXCI6XCI3NDU4OWJhYVwifSIsInIiOiJodHRwczovL2JqLmxpYW5qaWEuY29tL3dlbmRhL2xpZWJpYW8vYjEwMDEvIiwib3MiOiJ3ZWIiLCJ2IjoiMC4xIn0=',
            'Host': 'bj.lianjia.com',
            'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
            'sec-ch-ua-mobile': '?0',
            'Sec-Fetch-Dest': 'document',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Site': 'cross-site',
            'Sec-Fetch-User': '?1',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.72 Safari/537.36',
        }


# lpush com.asos.app:start_urls "women dress"
class SpeedSpider(RedisSpider):
    name = 'club.autohome.com.cn'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(SpeedSpider, self).__init__(*args, **kwargs)


    def make_requests_from_url(self, data:json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        return scrapy.Request(
            url=query,
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1, settings.page: 1},
            callback=self.parse,
            # dont_filter=False
        )

    def parse(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        page = response.meta[settings.page]
        #bprint(response.text)
        try:
            html = etree.HTML(response.text)
            title = html.xpath('//div[@class="q-desc"]/h1/text()')
            title = title[0] if title else ''
            comment_list = []
            com = html.xpath('//div[@class="content lj_editor_view"]')
            for i in com:
                link = i.xpath('.//p/text()')
                if len(link) == 0:
                    continue
                comment_list.append(' '.join(link))
            if page == 1:
                with open('/home/jinbiao/ceshi.txt', 'a', encoding='utf-8') as fp:
                    fp.write(title + '\n')
            for i in list(comment_list):
                with open('/home/jinbiao/ceshi.txt', 'a', encoding='utf-8') as fp:
                    fp.write(i + '\n')
            print(1111111, title)
            print(3333333, comment_list, type(list(comment_list)))
            for i in list(comment_list):
                print(44444444444, i)

            if page < 3:
                page += 1
                yield scrapy.Request(
                    url=query.replace('.html', '_{}.html'.format(page)),
                    headers=headers,
                    meta={settings.vers: vers, settings.query: query, settings.rank: rank, settings.page: page},
                    callback=self.parse,
                    dont_filter=False
                )
            # item = QuickSpiderItem()
            # make_item(self.name, item, '', vers)
            # item[settings.item_title] = title
            # item[settings.item_subtitle] = comment_list

            # if item[settings.item_title]:
            #     item[settings.item_rank] = rank
            #     rank = rank + 1
                # print(item)
                # yield item
            # else:
            #     return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)


        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)


if __name__ == "__main__":
    from scrapy import cmdline
    cmdline.execute('scrapy crawl club.autohome.com.cn'.split())
