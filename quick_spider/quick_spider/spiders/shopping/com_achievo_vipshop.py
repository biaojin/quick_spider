'com.achievo.vipshop'
# 唯品会

import json
import uuid

import requests
import scrapy
import datetime
import traceback
import math, random
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
from scrapy.utils.project import get_project_settings

headers = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
    # 'Content-Type': 'application/json',
}
data = {
'mobile_platform':'2',
'app_version':'4.23.2.20210325',
'wap_consumer':'A1',
'mobile_channel':'nature',
'client_type':'shop_quick_app',
'scene':'search',
'union_cid':'nature',
'context':'',
'client':'shop_quick_app',
'isPageLast':'false',
'ver':'2.0',
'fdc_area_id':'101101101',
'extParams':'{"preheatTipsVer":"3","exclusivePrice":"1","showSellPoint":1,"iconSpec":"3x","mclabel":1,"ic2label":1,"couponVer":"v2","futurePrice":"1"}',
'format':'json',
'warehouse':'VIP_BJ',
'standby_id':'nature',
'app_name':'shop_quick_app',
'mars_cid':'060fad85-eafa-5eca-be43-d70388e73f4f',
'vip_channel':'te',
't':'',
'productIds':'',
'api_key':'eb363d4ca7b944119519ff2038361be5',
'province_id':'101101',
'union_mark':'nature',
'source_app':'shop_quick_app',
}

# lpush com.asos.app:start_urls "women dress"
class VipshopDongpider(RedisSpider):
    name = 'com.achievo.vipshop'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(VipshopDongpider, self).__init__(*args, **kwargs)
        self.url = 'https://mapi-rp.vip.com/vips-mobile/rest/shopping/search/product/rank?app_name=shop_wap&app_version=4.0&api_key=8cec5243ade04ed3a02c5972bcda0d3f&mobile_platform=2&source_app=yd_wap&warehouse=VIP_BJ&fdc_area_id=101101101&province_id=101101&mars_cid={}_{}&mobile_channel=mobiles-%7C%7C&standby_id=quickapp_share&keyword={}&sort=0&channelId=1&wapConsumer=A1&gPlatform=WAP&functions=bsBrands%2CfavNumLabel%2CtotalLabel&mvip=true&_={}'

    def make_requests_from_url(self, data: json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        date_time = int(time.time() * 1000)
        b = '0123456789abcdef'
        c = ''
        for i in range(32):
            bb = math.ceil(1e8 * random.random()) % len(b)
            c += b[bb]
        return scrapy.Request(
            self.url.format(date_time, c, parse.quote(query),date_time),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query},
            callback=self.parse,
            dont_filter=True

        )

    def parse(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        date_time = int(time.time() * 1000)
        b = '0123456789abcdef'
        c = ''
        for i in range(32):
            bb = math.ceil(1e8 * random.random()) % len(b)
            c += b[bb]
        url = 'https://wxapi.appvipshop.com/vips-mobile/rest/shopping/product/module/list/v2?_xcxid={}'
        b = []
        try:
            response_data = json.loads(response.text).get('data', {}).get('products')
            print(response.text)

            if response_data is None:
                return_no_result(app_package_id=self.name, query=query, vers=vers)
            else:
                for a in response_data:
                        b.append(a.get('pid'))
                data['productIds'] = ','.join(b[:30])
                data['context'] = query
                data['t'] = str(date_time)
                # headers={
                #     'Accept-Language': 'zh-CN',
                #     'user-agent': 'Mozilla/5.0 (Linux; Android 10; ELE-AL00 Build/HUAWEIELE-AL00;)AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/18.0.1025 Mobile Safari/537.36 hap/1078/huawei com.huawei.fastapp/3.2.1.300 com.VIP.VIPQuickAPP/4.23.2.20210325 ({"packageName":"quickSearch","type":"other","extra":"{}"})',
                #     'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
                #     'Content-Length': '1166',
                #     'Host': 'wxapi.appvipshop.com',
                #     'Connection': 'Keep-Alive',
                #     'Accept-Encoding': 'gzip',
                # }
                yield scrapy.FormRequest(
                    url.format(date_time),
                    method='POST',
                    formdata=data,
                    headers=headers,
                    meta={settings.vers: vers, settings.query: query, settings.rank: 1,
                          settings.page: 1, 'headers': headers},
                    callback=self.parse_list,
                    dont_filter=True
                )

        except Exception as e:
            traceback.print_exc()
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)

    def parse_list(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        try:
            data = json.loads(response.text)
            data_list = data.get('data', {}).get('products', {})
            if len(data_list) == 0:
                return_no_result(app_package_id=self.name, query=query, vers=vers)
                return
            else:
                for con in data_list:
                    productId = con.get('productId')
                    if productId:
                        item_title = con.get('title','')
                        img_link = con.get('squareImage','')
                        price = con.get('price',{}).get('salePrice','')
                        vendorProductId = con.get('spuId')
                        brandid = con.get('brandId')
                        url = 'https://wxapi.appvipshop.com/vips-mobile/rest/shop/goods/vendorSkuList/v4?_xcxid={}&prepayMsgType=1&serviceTagVer=1&mobile_platform=2&functions=svipShowPrice%2CuserContext%2CbossChosenV2%2CbannerImageV2%2CpanelView%2CfuturePriceView%2CforeShowActive%2Csku_price%2Cactive_price%2Cprepay_sku_price%2Creduced_point_desc%2CsurprisePrice%2CbusinessCode%2CpromotionTips%2Cinvisible%2Cflash_sale_stock%2CbanInfo%2CpriceChart%2CpriceView%2CextraDetailImages%2CbuyLimit%2CfavStatus%2CquotaInfo%2CmidSupportServices%2CskuSupportServices%2CcouponAdTips%2CsvipAsSalePrice&app_version=4.23.2.20210325&svipPriceMode=1&wap_consumer=A1&mobile_channel=nature&mid={}&client_type=shop_quick_app&uiSwitch=priceViewAbt%3A1&union_cid=nature&brandid={}&commitmentVer=4&client=shop_quick_app&freightTipsVer=3&userContext=%7B%22uid%22%3A%220%22%2C%22ver%22%3A%2218%22%2C%22c%22%3A3%2C%22t%22%3A{}%2C%22uab%22%3A%7B%7D%2C%22switchExtData%22%3A%7B%224387%22%3A%7B%22r%22%3A%22A%22%2C%22v%22%3A%5B%22if_show_tips%3A2%22%5D%7D%2C%224453%22%3A%7B%22r%22%3A%22X%22%2C%22v%22%3A%5B%5D%7D%2C%227940%22%3A%7B%22r%22%3A%22B%22%2C%22v%22%3A%5B%5D%7D%2C%222032%22%3A%7B%22r%22%3A%22A%22%2C%22v%22%3A%5B%5D%7D%2C%224447%22%3A%7B%22r%22%3A%22B%22%2C%22v%22%3A%5B%5D%7D%2C%221913%22%3A%7B%22r%22%3A%22A%22%2C%22v%22%3A%5B%5D%7D%2C%224526%22%3A%7B%22r%22%3A%22A%22%2C%22v%22%3A%5B%5D%7D%2C%224280%22%3A%7B%22r%22%3A%22B%22%2C%22v%22%3A%5B%22isAutoGetCoupon%3A1%22%2C%22isAutoGetCouponForPreheat%3A1%22%5D%7D%2C%224307%22%3A%7B%22r%22%3A%22B%22%2C%22v%22%3A%5B%5D%7D%2C%227915%22%3A%7B%22r%22%3A%22A%22%2C%22v%22%3A%5B%5D%7D%7D%7D&supportSquare=1&panelViewVer=2&ver=2.0&fdc_area_id=101101101&supportAllPreheatTipsTypes=1&openSvipMode=0&salePriceVer=2&format=json&warehouse=VIP_BJ&standby_id=nature&isUseMultiColor=1&promotionTipsVer=5&vendorProductId={}&app_name=shop_quick_app&mars_cid=060fad85-eafa-5eca-be43-d70388e73f4f&salePriceTypeVer=2&vip_channel=te&t=1618290504&api_key=eb363d4ca7b944119519ff2038361be5&province_id=101101&union_mark=nature&source_app=shop_quick_app&couponInfoVer=2&device=3&priceViewVer=8'
                        rank+=1
                        yield scrapy.Request(
                            url.format(int(time.time()*1000), productId,brandid, int(time.time()),vendorProductId),
                            headers=headers,
                            meta={settings.vers: vers, settings.query: query, settings.rank: rank, 'item_title': item_title, 'img_link': img_link, 'price': price, 'productId':productId, 'brandId':brandid},
                            callback=self.parse_datas,
                            # dont_filter=False
                            )
                    else:
                        return
        except Exception as e:
            traceback.print_exc()
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)

    def parse_datas(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        productId = response.meta['productId']
        brandId = response.meta['brandId']
        try:
            data = json.loads(response.text)
            item = QuickSpiderItem()
            make_item(self.name, item, query, vers)
            # 标题
            item[settings.item_title] = response.meta['item_title']
            # 价格
            item[settings.item_price] = response.meta['price']
            # 图片
            item[settings.item_image_link] = response.meta['img_link']
            # 详情页
            item_detail_link = 'https://m.vip.com/product-{}-{}.html?msns=weixin_mina-4.23.3.20210408-wx&st=p-url&cid=b8dd720e-b423-5016-8d8f-e42568d4d0d9&chl_param=share%3AV97aTAkYSX&abtid=13&uid=453231240&mars_cid_a=b8dd720e-b423-5016-8d8f-e42568d4d0d9&chl_type=share&f=quickapp_share'.format(brandId,productId)
            if item_detail_link and item_detail_link is not None:
                item[settings.item_detail_link] = item_detail_link
            else:
                item[settings.item_detail_link] = ''
            subtitle= data.get("data", {}).get('product_price_range_mapping',{}).get(productId,{}).get('priceView',{}).get('saleLabel')
            if subtitle:
                item[settings.item_subtitle] = subtitle.replace('累计热卖', '已售')
            else:
                item[settings.item_subtitle] = ''
            item[settings.item_colour] = ''
            # 简介标签
            # item[settings.item_subtitle] = self.tag_introduction(item[settings.item_detail_link])
            item[settings.item_Twolabel] = '电子产品'
            item[settings.item_Onelabel] = '购物'
            item[settings.item_appType] = 'quick'

            if item[settings.item_title] and item[settings.item_detail_link]:
                item[settings.item_rank] = rank
                yield item

        except Exception as e:
            traceback.print_exc()
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)


if __name__ == "__main__":
    from scrapy import cmdline

    cmdline.execute('scrapy crawl com.achievo.vipshop'.split())
