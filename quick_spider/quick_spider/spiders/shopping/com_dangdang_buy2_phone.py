
import json
import uuid
import scrapy
import datetime
import random
import traceback
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {
        'content-type': 'application/x-www-form-urlencoded',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 10; V1914A Build/QP1A.190711.020; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/78.0.3904.96 Mobile Safari/537.36 hap/1.8/vivo com.vivo.hybrid/1.8.4.802 com.dangdang.quickapp/1.7.3 ({"packageName":"com.quickapp.center","type":"center_special_result","extra":{"original":{"packageName":"com.vivo.globalsearch","type":"globalsearch_suggest_floor","extra":{}},"third_st_param":"{\"source_version\":46}","scene":"dialog"}})',
        'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8',
        'Host': 'api.dangdang.com',
        'Connection': 'Keep-Alive',
        'Accept-Encoding': 'gzip',
    }
phono_id = {
    '华为': '5107',
    'OPPO': '1075',
    '小米': '6546',
    'vivo': '18959',
    '苹果': '2276',
}


# lpush com.asos.app:start_urls "women dress"
class DushemovieBrandSpider(RedisSpider):
    name = 'com.dangdang.buy2_phono'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(DushemovieBrandSpider, self).__init__(*args, **kwargs)
        self.url = 'https://api.dangdang.com/mapi7/search/all-search?udid=f72831e7cfc46ed7ca8f3a411cee82c9&permanent_id=20210407143853397919054045330574793&client_version=1.0&user_client=huawei&unionId=580-100423-center-quickApp&openid=ce0d9c391e615865&province_id=111&city_id=1&district_id=1110101&town_id=&c=search&a=all-search&keyword=&page_action=search&page_version=new2&img_size=e&result_set_all_count=10&brandid={}&sort_type=default_0&sort_name=%E7%BB%BC%E5%90%88&show_shop=0&page={}&cid=4004279&pop=-1&load=1&timestamp=1619069097951&time_code=ae38947d156cf373ddecd6ec7e195287'

    def make_requests_from_url(self, data: json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        return scrapy.FormRequest(
            self.url.format(phono_id.get(query), 1),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1,
                  settings.page: 1},
            callback=self.parse,
            # dont_filter=False
        )

    def parse(self, response):
        query = response.meta[settings.query]
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        page = response.meta[settings.page]
        try:
            response = json.loads(response.text)
            if len(response.get('data', {}).get('product', '')) == 0:
                return_no_result(app_package_id=self.name, query=query, vers=vers)
            else:
                for product in response.get('data', {}).get('product'):

                    item = QuickSpiderItem()
                    make_item(self.name, item, query, vers)
                    # 名称
                    item[settings.item_title] = product.get('productName', '')
                    # 详情页链接
                    id = product.get('productId', '')
                    if id and len(id) != 0:
                        item[settings.item_detail_link] = 'http://product.dangdang.com/{}.html'.format(id)
                    else:
                        item[settings.item_detail_link] = ''
                    # 图片
                    item[settings.item_image_link] = product.get('productImg', '')
                    # 价格
                    item[settings.item_price] = product.get('price', '')
                    # 评价
                    item[settings.item_Comment] = product.get('commentCount', '').replace('评价', '').replace('暂无', '')
                    # 好评
                    item[settings.item_Praise] = product.get('goodCommentRate', '').replace('好评', '').replace('暂无', '')
                    # 店铺名称
                    item[settings.item_name] = product.get('shopName', '')

                    item[settings.item_type] = query
                    item[settings.item_Twolabel] = '电子产品'
                    item[settings.item_Onelabel] = '购物'
                    item[settings.item_class] = '手机'
                    item[settings.server_type] = '泛化'
                    item[settings.item_appType] = 'quick'
                    if item[settings.item_title]:
                        item[settings.item_rank] = rank
                        rank = rank + 1
                        yield item
                # else: return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)
                if page < 8:
                    page += 1
                    yield scrapy.Request(
                        self.url.format(phono_id.get(query), page),
                        headers=headers,
                        meta={settings.vers: vers, settings.query: query, settings.rank: rank, settings.page: page},
                        callback=self.parse,
                        dont_filter=False
                    )

        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)


if __name__ == "__main__":
    from scrapy import cmdline

    cmdline.execute('scrapy crawl com.dangdang.buy2_phono'.split())
