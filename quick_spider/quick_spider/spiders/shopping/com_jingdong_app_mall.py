# 京东

import json
import uuid

import requests
import scrapy
import datetime
import random
import traceback
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
from scrapy.utils.project import get_project_settings

headers = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
    # 'Content-Type': 'application/json',
}


# lpush com.asos.app:start_urls "women dress"
class JingDongpider(RedisSpider):
    name = 'com.jingdong.app.mall'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(JingDongpider, self).__init__(*args, **kwargs)
        self.url = 'https://so.m.jd.com/ware/search.action?keyword={}&searchFrom=home&sf=11&as=1'

    def make_requests_from_url(self, data: json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)

        return scrapy.Request(
            self.url.format(parse.quote(query)),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1,
                  settings.page: 1, 'headers': headers},
            callback=self.parse,
            dont_filter=True

        )

    def parse(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        try:
            traceids = re.findall(r"window.traceid = '(.*?)';</script>", response.text, re.S)
            if len(traceids) != 0:
                traceid = traceids[0]
            else:
                traceid = '1156829551455853227'
            url = 'https://so.m.jd.com/ware/search._m2wq_list?keyword={}&datatype=1&callback=jdSearchResultBkCbH&page={}&pagesize=10&ext_attr=no&brand_col=no&price_col=no&color_col=no&size_col=no&ext_attr_sort=no&merge_sku=yes&multi_suppliers=yes&area_ids=1,72,2819&filt_type=redisstore,1;&qp_disable=no&fdesc={}&t1={}&traceid={}'
            yield scrapy.Request(
                url.format(parse.quote(query), 1, parse.quote('北京'), int(time.time()*1000), traceid),
                headers=headers,
                meta={settings.vers: vers, settings.query: query, settings.rank: 1,
                      settings.page: 1, 'traceid': traceid},
                callback=self.parse_data,
                dont_filter=True

            )

        except Exception as e:
            traceback.print_exc(
            )
    def parse_data(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        page = response.meta[settings.page]
        try:
            traceid = response.meta['traceid']
            response_data = re.findall(r'jdSearchResultBkCbH\((.*?)}\)', response.text, re.S)
            if len(response_data) != 0:

                data = json.loads(response_data[0]+'}')
            else:
                ErrorSignal.main(self.name, '京东返回内容解析出错', query, error=None, vers=vers)
                return
            data_list = data.get('data', {}).get('searchm', {}).get('Paragraph')
            if len(data_list) == 0:
                return_no_result(app_package_id=self.name, query=query, vers=vers)
                return
            else:
                for con in data_list:
                    item = QuickSpiderItem()
                    make_item(self.name, item, query, vers)
                    # 标题
                    item_title = con.get('Content').get('warename')
                    if item_title and item_title is not None:
                        item[settings.item_title] = item_title
                    else:
                        item[settings.item_title] = ''

                    # 详情页
                    item_detail_link = con.get('toItemLink')
                    if item_detail_link and item_detail_link is not None:
                        item[settings.item_detail_link] = item_detail_link
                    else:
                        item[settings.item_detail_link] = ''

                    # 图片
                    item_image_link = con.get('Content', {}).get('imageurl')
                    if item_image_link and item_image_link is not None:
                        item[settings.item_image_link] = 'https://m.360buyimg.com/mobilecms/s750x750_' + item_image_link
                    else:
                        item[settings.item_image_link] = ''

                    subtitle = con.get('shop_name')
                    if subtitle:
                        item[settings.item_subtitle] = subtitle
                    else:
                        item[settings.item_subtitle] = ''
                    item_other = con.get('Content', {}).get('extname')
                    if item_other:
                        item[settings.item_other] = item_other
                    else:
                        item[settings.item_other] = ''
                    # 价格
                    item_price = con.get('dredisprice')
                    if item_price and item_price is not None:
                        item[settings.item_price] = item_price
                    else:
                        item[settings.item_price] = ''

                    # 默认颜色选项
                    item_colour = con.get('Content').get('color')
                    if item_colour and item_colour is not None:
                        item[settings.item_colour] = item_colour
                    else:
                        item[settings.item_colour] = ''
                    # 好评度
                    item_Praise = con.get('good')
                    if item_Praise:
                        item[settings.item_Praise] = con.get('good')+'%'
                    else:
                        item[settings.item_Praise] = ''
                    # 评价标签词,
                    # 评论,
                    item_id = con.get('product_id')
                    # if item_id and item_id is not None and item_id !=0:
                    #     item[settings.item_Comment_tags], item[
                    #         settings.item_Comment] = self.Comment_tags(item_id)
                    #else:
                    item[settings.item_Comment_tags], item[settings.item_Comment] = '', '0'

                    # 简介标签
                    # item[settings.item_subtitle] = self.tag_introduction(item[settings.item_detail_link])
                    item[settings.item_Twolabel] = '电子产品'
                    item[settings.item_Onelabel] = '购物'

                    item[settings.item_appType] = 'quick'

                    if item[settings.item_title] and item[settings.item_detail_link]:
                        item[settings.item_rank] = rank
                        rank = rank + 1
                        yield item

            # if page < 3:
            #     page += 1
            #     url = 'https://so.m.jd.com/ware/search._m2wq_list?keyword={}&datatype=1&callback=jdSearchResultBkCbH&page={}&pagesize=10&ext_attr=no&brand_col=no&price_col=no&color_col=no&size_col=no&ext_attr_sort=no&merge_sku=yes&multi_suppliers=yes&area_ids=1,72,2819&filt_type=redisstore,1;&qp_disable=no&fdesc={}&t1={}&traceid={}'
            #     yield scrapy.Request(
            #             url.format(parse.quote(query), page, parse.quote('北京'), int(time.time()*1000), traceid),
            #             headers=headers,
            #             meta={settings.vers: vers, settings.query: query, settings.rank: rank,settings.page: page, 'headers': headers,'traceid':traceid},
            #             callback=self.parse_data,
            #             dont_filter=False
            #
            #         )

        except Exception as e:
            print(e)
            traceback.print_exc()
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)

    # def tag_introduction(self, linkurl):
    #     date = requests.get(linkurl, headers=headers).text
    #     try:
    #         data = json.loads(
    #             re.sub('\s+', '', date.replace('\n', '')).split('"AdvertCount":')[1].split(',"pingou":')[0].replace('\\',                                                                                             '/'))
    #         item_subtitle = data.get('ad').split('/')[0]
    #         return item_subtitle
    #     except Exception as e:
    #         print(linkurl)
    #         traceback.print_exc()
    #         return ''
    """
    京东评论数为单独接口， 请求次数过多返回的结果为空，需要使用优质代理，pc端接口返回的数据评论数不是具体数值，h5和快应用返回的数据为具体数值，
    """

    def Comment_tags(self, ta_url):
        tag_headers = {
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.72 Safari/537.36',
            'referer': 'https://item.m.jd.com/',
            'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
            'sec-ch-ua-mobile': '?0',
            'sec-fetch-dest': 'script',
            'sec-fetch-mode': 'no-cors',
            'sec-fetch-site': 'same-site',
        }
        time.sleep(1)

        date = requests.get(
            "https://wq.jd.com/commodity/comment/getcommentlist?callback=skuJDEvalA&version=v2&pagesize=10&sceneval=2&score=0&sku={}&sorttype=5&page=1&t={}".format(ta_url,random.random()),
            proxies={
                "http": "http://e009fc666edb4f9d8ad928b742ab71c0:@proxy.crawlera.com:8011/",
            },
            headers=tag_headers,
            verify=False
        ).text
        # date = requests.get(ta_url, headers=tag_headers, verify=False).text
        try:
            # print(date)
            commenttags = []
            data = json.loads(date.replace('skuJDEvalA(', '').replace('})', '}').replace('\\', ''))
            tags = data.get('result', {}).get('hotCommentTagStatistics')
            Good = data.get('result', {}).get('productCommentSummary', {}).get('GoodRate')
            #GoodRate = "%.0f%%" % (float(Good) * 100)
            CommentCount = data.get('result', {}).get('productCommentSummary', {}).get('CommentCount')
            for tag_ in tags:
                tag = tag_.get('name')
                num = tag_.get('count')
                con = '{} ({})'.format(tag, num)
                commenttags.append(con)
            return ' ,'.join(commenttags), str(CommentCount)

        except Exception as e:
            print(11111111111111111111111, ta_url)
            traceback.print_exc()


if __name__ == "__main__":
    from scrapy import cmdline

    cmdline.execute('scrapy crawl com.jingdong.app.mall'.split())
