"""
添加字段
item_type ：喜剧
item_class：热剧， 热片
server_type : 泛化

"""

# 叮当快药

import json
import uuid
import scrapy
import datetime
import random
import requests
import traceback

from scraper_api import ScraperAPIClient
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36',
    'x-requested-with': 'XMLHttpRequest'}

url_list = {'东城': '/ershoufang/dongcheng/',
            '西城': '/ershoufang/xicheng/',
            '朝阳': '/ershoufang/chaoyang/',
            '海淀': '/ershoufang/haidian/',
            '丰台': '/ershoufang/fengtai/',
            '石景山': '/ershoufang/shijingshan/',
            '通州': '/ershoufang/tongzhou/',
            '昌平': '/ershoufang/changping/',
            '大兴': '/ershoufang/daxing/',
            '亦庄开发区': '/ershoufang/yizhuangkaifaqu/',
            '顺义': '/ershoufang/shunyi/',
            '房山': '/ershoufang/fangshan/',
            '门头沟': '/ershoufang/mentougou/',
            '平谷': '/ershoufang/pinggu/',
            '怀柔': '/ershoufang/huairou/',
            '密云': '/ershoufang/miyun/',
            '延庆': '/ershoufang/yanqing/',}


# lpush com.asos.app:start_urls "women dress"
class DushemovieBrandSpider(RedisSpider):
    name = 'com.lianjia.beike'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(DushemovieBrandSpider, self).__init__(*args, **kwargs)
        self.url = 'https://bj.ke.com{}pg{}/'

    def make_requests_from_url(self, data: json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        return scrapy.Request(
            self.url.format(url_list.get(query), 1),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1, settings.page: 1, 'headers': headers},
            callback=self.parse,
            # dont_filter=False
        )

    def parse(self, response):
        query = response.meta[settings.query]
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        page = response.meta[settings.page]
        headers = response.meta['headers']
        try:
            response = etree.HTML(response.text).xpath('//ul[@class="sellListContent"]//li')
            if len(response) == 0:
                return_no_result(app_package_id=self.name, query=query, vers=vers)
            else:
                for product in response:

                    item = QuickSpiderItem()
                    make_item(self.name, item, '', vers)
                    # 名称
                    item_title = product.xpath('./div/div[1]/a//text()')
                    item[settings.item_title] = item_title[0] if item_title and len(item_title) > 0 else ''
                    # 简介
                    item_subtitle = product.xpath('./div/div[2]/div[2]/text()')
                    item[settings.item_subtitle] = ''.join(item_subtitle).replace('\n', '').replace(' ', '') if item_subtitle and len(item_subtitle) > 0 else ''
                    # 详情页链接
                    item_detail_link = product.xpath('./div/div[1]/a/@href')
                    item[settings.item_detail_link] = item_detail_link[0] if item_detail_link and len(item_detail_link) > 0 else ''
                    # 图片
                    item_image_link = product.xpath('./a[1]/img/@data-original')
                    item[settings.item_image_link] = item_image_link[0] if item_image_link and len(item_image_link) > 0 else ''
                    # 时间
                    item_time = product.xpath('./div/div[2]/div[3]/text()')
                    item[settings.item_time] = ''.join(item_time).replace('\n', '').replace(' ', '') if item_time and len(item_time) > 0 else ''
                    # 小区名
                    item_name = product.xpath('./div/div[2]/div[1]/div/a//text()')
                    item[settings.item_name] = ''.join(item_name).replace('\n', '').replace(' ', '') if item_name and len(item_name) > 0 else ''
                    # 价格
                    item_price = product.xpath('./div/div[2]/div[5]//text()')
                    item[settings.item_price] = ''.join(item_price).replace('\n', '').replace(' ', '') if item_price and len(item_price) > 0 else ''
                    # 标签
                    item_Comment_tags = product.xpath('./div/div[2]/div[4]//text()')
                    item[settings.item_Comment_tags] = ' '.join(item_Comment_tags).replace('\n', '').replace(' ', '').strip() if item_Comment_tags and len(item_Comment_tags) > 0 else ''


                    item[settings.item_type] = query
                    item[settings.item_class] = '二手房'
                    item[settings.server_type] = '泛化'
                    item[settings.item_Twolabel] = '二手房 买房'
                    item[settings.item_Onelabel] = '购物 娱乐'
                    item[settings.item_appType] = 'quick'
                    print(1111111111, item[settings.item_title])
                    if item[settings.item_title]:
                        item[settings.item_rank] = rank
                        rank = rank + 1
                        yield item
                # else: return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)
                if page < 30:
                    print('0000000000000')
                    page += 1
                    # print(11111111, query, rank)
                    yield scrapy.Request(
                        self.url.format(url_list.get(query), page),
                        headers=headers,
                        meta={settings.vers: vers, settings.query: query, settings.rank: rank, settings.page: page, 'headers': headers},
                        callback=self.parse,
                        # dont_filter=False
                    )

        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            if page > 100: mg = False
            ErrorSignal.main(self.name, str(traceback.format_exc()), '第{}页报错 {}'.format(page, query), e, vers)


if __name__ == "__main__":
    from scrapy import cmdline

    cmdline.execute('scrapy crawl com.lianjia.beike'.split())
