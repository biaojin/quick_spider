# 叮当快药

import json
import uuid
import scrapy
import datetime
import random
import traceback
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {
    'user-agent': 'Mozilla/5.0 (Linux; Android 10; ELE-AL00 Build/HUAWEIELE-AL00;)AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/18.0.1025 Mobile Safari/537.36 hap/1078/huawei com.huawei.fastapp/3.2.1.300 com.ddky.compzd/2.8.8 ({"packageName":"search","type":"url","extra":"{}"})',

}


# lpush com.asos.app:start_urls "women dress"
class ZolSpider(RedisSpider):
    name = 'com.zol.android'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(ZolSpider, self).__init__(*args, **kwargs)
        self.url = 'https://wap.zol.com.cn/index.php?c=List&keyword={}'

    def make_requests_from_url(self, data: json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        return scrapy.Request(
            self.url.format(parse.quote(query)),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1,
                  settings.page: 1, 'headers': headers},
            callback=self.parse_linkurl,
            # dont_filter=False
        )

    def parse_linkurl(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        try:
            data = etree.HTML(response.text).xpath('//ul[@class="product-list"]/li')
            if len(data) == 0:
                return_no_result(app_package_id=self.name, query=query, vers=vers)
            else:
                for con in data:
                    link_url = con.xpath('.//div/a/@href')
                    if len(link_url) >= 1:
                        link_url = 'https://wap.zol.com.cn' + link_url[0]
                        yield scrapy.Request(
                            link_url,
                            headers=headers,
                            meta={settings.vers: vers, settings.query: query, settings.rank: rank, settings.page: 1,
                                  'headers': headers},
                            callback=self.parse,
                            dont_filter=True
                        )

        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)

    def parse(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        link_url = ''
        try:
            con = etree.HTML(response.text)
            responses = con.xpath('//ul[@class="news-list pl pr p15"]/li')
            if 'article.html' in response.url:
                responses = con.xpath('//ul[@id="evaluatingList"]/li')
            data_url = con.xpath('//a[@class="icon-right fr"]')
            for i in data_url:
                name = i.xpath('./text()')
                url = i.xpath('./@href')
                if len(name) != 0 and '更多资讯' in name:
                    link_url = 'https://wap.zol.com.cn' + url[0]

            if len(responses) == 0:
                pass
                # return_no_result(app_package_id=self.name, query=query, vers=vers)
            else:
                for product in responses:

                    item = QuickSpiderItem()
                    make_item(self.name, item, query, vers)

                    item[settings.item_Onelabel] = '知识'
                    item[settings.item_Twolabel] = '科技'
                    item[settings.item_appType] = 'quick'
                    item_detail_link = product.xpath('.//a/@href')
                    if len(item_detail_link) >= 1 and item_detail_link[0].startswith('//'):
                        item[settings.item_detail_link] = 'https:' + item_detail_link[0]
                    elif len(item_detail_link) >= 1 and item_detail_link[0].startswith('/'):
                        item[settings.item_detail_link] = 'https://wap.zol.com.cn' + item_detail_link[0]
                    elif len(item_detail_link) >= 1 and item_detail_link[0].startswith('http'):
                        item[settings.item_detail_link] = item_detail_link[0]
                    else:
                        item[settings.item_detail_link] = ''
                    item_time = product.xpath('.//span[@class="time"]/text()')
                    if len(item_time) >= 1:
                        item[settings.item_time] = item_time[0]
                    else:
                        item_time = product.xpath('.//span[@class="date"]/text()')
                        if len(item_time) >= 1:
                            if len(item_time[0]) > 5:
                                item[settings.item_time] = item_time[0]
                            else:
                                item[settings.item_time] = datetime.datetime.now().strftime('%Y-') + item_time[0]
                        else:
                            item[settings.item_time] = ''

                    item_title = product.xpath('.//a/div/h3/text()')
                    item[settings.item_title] = item_title[0] if len(item_title) >= 1 else ''

                    item_Comment = product.xpath('.//span[@class="comment-num"]/text()')
                    if len(item_Comment) >= 1:
                        item[settings.item_Comment] = item_Comment[0]
                    else:
                        item_Comment = product.xpath('.//span[@class="review-num"]/text()')
                        if len(item_Comment) >= 1:
                            item[settings.item_Comment] = item_Comment[0]
                        else:
                            item[settings.item_Comment] = ''

                    item_image_link = product.xpath('.//a/figure[1]/img/@data-src')
                    if len(item_image_link) >= 1:
                        item[settings.item_image_link] = item_image_link[0]
                    else:
                        item_image_link = product.xpath('.//a/div/img/@data-lazy-src')
                        if len(item_image_link) >= 1:
                            item[settings.item_image_link] = item_image_link[0]
                        else:
                            item[settings.item_image_link] = ''

                    if item[settings.item_title] and item[settings.item_image_link] and item[settings.item_detail_link]:
                        item[settings.item_rank] = rank
                        rank = rank + 1
                        yield item
                if link_url:
                    yield scrapy.Request(
                        link_url,
                        headers=headers,
                        meta={settings.vers: vers, settings.query: query, settings.rank: rank, settings.page: 1,
                              'headers': headers},
                        callback=self.parse,
                        dont_filter=True
                    )

                # else: return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)


        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)


if __name__ == "__main__":
    from scrapy import cmdline

    cmdline.execute('scrapy crawl com.zol.android'.split())
'https://m.zol.com.cn/article/7647679.html'