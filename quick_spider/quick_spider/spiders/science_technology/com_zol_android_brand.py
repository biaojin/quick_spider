"""
添加字段
item_type ：喜剧
item_class：热剧， 热片
server_type : 泛化

"""

# 中关村在线

import json
import uuid
import scrapy
import datetime
import random
import traceback
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {
    'user-agent': 'Mozilla/5.0 (Linux; Android 10; ELE-AL00 Build/HUAWEIELE-AL00;)AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/18.0.1025 Mobile Safari/537.36 hap/1078/huawei com.huawei.fastapp/3.2.1.300 com.ddky.compzd/2.8.8 ({"packageName":"search","type":"url","extra":"{}"})',

}
# zol_id = {'笔记本': ['nb', 'https://m.zol.com.cn/nb/ajax/listAjax.php?subNav=index&page={}'],
#           '手机': ['mobile', 'https://m.zol.com.cn/mobile/ajax/mobileListAjax.php?subNav=index&subSmallNav=&page={}&referrer=https%3A%2F%2Fm.zol.com.cn%2F&uid=58%2BJ4fj1j7QuMDg4NjIyLjE2MTc2ODk2NTk%253D'],
#           '平板': ['pad','https://m.zol.com.cn/integratedChannel/ajax/channeListAjax.php?tag=pad&moduName=index&view_type=0&page={}'],
#           }


# lpush com.asos.app:start_urls "women dress"
class DushemovieBrandSpider(RedisSpider):
    name = 'com.zol.android_brand'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(DushemovieBrandSpider, self).__init__(*args, **kwargs)
        self.url = 'https://m.zol.com.cn/article/search.php?kword={}&view_type=3'

    def make_requests_from_url(self, data: json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        return scrapy.Request(
            self.url.format(parse.quote(query)),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1, settings.page: 1, 'headers': headers},
            callback=self.parse,
            # dont_filter=False
        )

    def parse(self, response):
        query = response.meta[settings.query]
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        page = response.meta[settings.page]
        try:
            responses = etree.HTML(response.text).xpath('//ul[@class="news-list"]/li')
            if len(responses) == 0:
                return_no_result(app_package_id=self.name, query=query, vers=vers)
            else:
                for product in responses:

                    item = QuickSpiderItem()
                    make_item(self.name, item, query, vers)
                    # 名称
                    item_title = product.xpath('.//h3/text()')
                    item[settings.item_title] = item_title[0] if len(item_title) >= 1 else ''
                    # 详情页链接
                    link = product.xpath('.//a/@href')
                    item[settings.item_detail_link] = 'https://m.zol.com.cn' + link[0] if len(link) >= 1 else ''
                    # 图片
                    img = product.xpath('.//img/@data-src')
                    if len(img) == 0:
                        img = product.xpath('.//img/@src')
                    if len(img) == 0:
                        item[settings.item_image_link] = ''
                    else:
                        item[settings.item_image_link] = img[0]

                    # 时间
                    item_time = product.xpath('.//span[@class="time"]/text()')
                    if item_time and len(item_time) >= 1:
                        item[settings.item_time] = extraction_time(item_time[0])
                    else:
                        item[settings.item_time] = ''
                    # 评论
                    item_Comment = product.xpath('.//span[@class="comment-num"]/text()')
                    if item_Comment and len(item_Comment) >= 1:
                        item[settings.item_Comment] = item_Comment[0].replace('万次', '').split('.')[0]
                    else:
                        item[settings.item_Comment] = ''

                    item[settings.item_class] = query
                    item[settings.server_type] = '泛化'
                    item[settings.item_Onelabel] = '知识'
                    item[settings.item_Twolabel] = '科技'
                    item[settings.item_appType] = 'quick'
                    if item[settings.item_title]:
                        item[settings.item_rank] = rank
                        rank = rank + 1
                        yield item
                # else: return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)
                # if page < 4:
                #     page += 1
                #     yield scrapy.Request(
                #         url=zol_id.get(query)[1].format(page),
                #         headers=headers,
                #         meta={settings.vers: vers, settings.query: query, settings.rank: rank, settings.page: page, 'headers': headers},
                #         callback=self.parse,
                #         dont_filter=False
                #     )

        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)


if __name__ == "__main__":
    from scrapy import cmdline

    cmdline.execute('scrapy crawl com.zol.android_brand'.split())
