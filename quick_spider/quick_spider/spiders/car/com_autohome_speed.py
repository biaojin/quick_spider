#com.autohome.speed
import json
import uuid
import scrapy
import datetime
import random
import traceback
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {
'user-agent': 'Mozilla/5.0 (Linux; Android 10; ELE-AL00 Build/HUAWEIELE-AL00;)AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/18.0.1025 Mobile Safari/537.36 hap/1078/huawei com.huawei.fastapp/3.2.1.300 com.ddky.compzd/2.8.8 ({"packageName":"search","type":"url","extra":"{}"})',
           }


# lpush com.asos.app:start_urls "women dress"
class SpeedSpider(RedisSpider):
    # 汽车之家急速版
    name = 'com.autohome.speed'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(SpeedSpider, self).__init__(*args, **kwargs)
        self.url = 'https://openapi.autohome.com.cn/autohome/uc-news-quickappservice/msapi/sou/searcharticlenew?q={}&pf=rn&pagesize=20&pageindex={}&_appid=car&_sign=530995DAE4682CA539874CFC3E81AE4B&_timestamp={}'

    def make_requests_from_url(self, data:json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        return scrapy.Request(
            self.url.format(parse.quote(query), 1, int(time.time())),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1, settings.page: 1},
            callback=self.parse,
            # dont_filter=False
        )

    def parse(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        page = response.meta[settings.page]
        #bprint(response.text)
        try:
            data = json.loads(response.text).get('result', {})
            if len(data.get('hitlist')) == 0 and page == 1:
                return_no_result(app_package_id=self.name, query=query, vers=vers)
            else:
                for product in data.get('hitlist'):
                    item = QuickSpiderItem()
                    make_item(self.name, item, query, vers)
                    item_title = product.get('data').get('title', '')
                    if item_title:
                        item[settings.item_title] = item_title
                    else:
                        item[settings.item_title] = ''
                    item_detail_link = product.get('data').get('url', '')
                    if item_detail_link:
                        item[settings.item_detail_link] = item_detail_link
                    else:
                        item[settings.item_detail_link] = ''

                    # 图片
                    image_link_list = []
                    image_link_1 = product.get('data').get('FirstCoverImg')
                    if image_link_1:
                        image_link_list.append(image_link_1)
                    image_link_2 = product.get('data').get('SecondCoverImg')
                    if image_link_2:
                        image_link_list.append(image_link_2)
                    image_link_3 = product.get('data').get('ThirdCoverImg')
                    if image_link_3:
                        image_link_list.append(image_link_3)
                    if len(image_link_list) != 0:
                        item[settings.item_image_link] = image_link_list
                    else:
                        item[settings.item_image_link] = ''
                    item_subtitle = product.get('data').get('stitle')
                    if item_subtitle:
                        item[settings.item_subtitle] = item_subtitle
                    else:
                        item[settings.item_subtitle] = ''
                    item_time = product.get('data').get('date')
                    if item_time:
                        item[settings.item_time] = item_time.split(' ')[0]
                    else:
                        item[settings.item_time] = ''
                    item_type = product.get('data').get('class_name')
                    if item_type:
                        item[settings.item_type] = item_type
                    else:
                        item[settings.item_type] = ''
                    item[settings.item_Onelabel] = '知识'
                    item[settings.item_Twolabel] = '汽车'
                    item[settings.item_appType] = 'quick'
                    if item[settings.item_title] and item[settings.item_image_link]:
                        item[settings.item_rank] = rank
                        rank = rank + 1
                        # print(item)
                        yield item
                    # else:
                    #     return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)


        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)
        if page < 2:
            page += 1
            yield scrapy.Request(
            self.url.format(parse.quote(query),page,int(time.time())),
            headers=headers,
            meta={settings.vers: vers, settings.query: query, settings.rank: rank, settings.page: page},
            callback=self.parse,
            dont_filter=True
        )


if __name__ == "__main__":
    from scrapy import cmdline
    cmdline.execute('scrapy crawl com.autohome.speed'.split())
