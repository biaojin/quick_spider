#com.xcar.activity

import json
import uuid
import scrapy
import datetime
import random
import traceback
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {
'user-agent': 'Mozilla/5.0 (Linux; Android 10; ELE-AL00 Build/HUAWEIELE-AL00;)AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/18.0.1025 Mobile Safari/537.36 hap/1078/huawei com.huawei.fastapp/3.2.1.300 com.ddky.compzd/2.8.8 ({"packageName":"search","type":"url","extra":"{}"})',
           }


# lpush com.asos.app:start_urls "women dress"
class XcarSpider(RedisSpider):
    # 爱卡汽车
    name = 'com.xcar.activity_car'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(XcarSpider, self).__init__(*args, **kwargs)
        self.url = 'https://mp.xcar.com.cn/api/search-car?_t={}&pagesize=20&cityid=475&page={}&word={}&channelId=undefined'

    def make_requests_from_url(self, data:json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        self.flge = True
        return scrapy.Request(
            self.url.format(int(time.time()*1000), 1, parse.quote(query)),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1, settings.page: 1},
            callback=self.parse,
            # dont_filter=False
        )

    def parse(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        page = response.meta[settings.page]
        #bprint(response.text)
        try:
            data = json.loads(response.text).get('data')
            if len(data) == 0:
                print(query, page)
                print(response.url)
                self.flge = False
                return_no_result(app_package_id=self.name, query=query, vers=vers)
                return
            else:
                for product in data:
                    item = QuickSpiderItem()
                    make_item(self.name.split('_')[0], item, query, vers)
                    item_title = product.get('psname', '')
                    if item_title:
                        item[settings.item_title] = item_title
                    else:
                        item[settings.item_title] = ''
                    item_detail_link = product.get('pserid', '')
                    if item_detail_link:
                        item[settings.item_detail_link] = 'https://a.xcar.com.cn/{}'.format(item_detail_link)
                    else:
                        item[settings.item_detail_link] = ''

                    # 图片
                    image_link = product.get('img_url')
                    if image_link:
                        item[settings.item_image_link] = image_link
                    else:
                        item[settings.item_image_link] = ''
                    item_subtitle = product.get('auto_type')
                    if item_subtitle:
                        item[settings.item_subtitle] = item_subtitle
                    else:
                        item[settings.item_subtitle] = ''
                    item_price = product.get('price')
                    if item_price:
                        item[settings.item_price] = item_price
                    else:
                        item[settings.item_time] = ''
                    item[settings.item_type] = query.replace('汽车', '')
                    item[settings.item_class] = '汽车'
                    item[settings.item_Onelabel] = '知识'
                    item[settings.item_Twolabel] = '汽车'
                    item[settings.item_appType] = 'quick'
                    if item[settings.item_title] and item[settings.item_image_link]:
                        item[settings.item_rank] = rank
                        rank = rank + 1
                        # print(item)
                        yield item
                    # else:
                    #     return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)


        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)
        if self.flge:
            page += 1
            yield scrapy.Request(
            self.url.format(int(time.time()*1000), page, parse.quote(query)),
            headers=headers,
            meta={settings.vers: vers, settings.query: query, settings.rank: rank, settings.page: page},
            callback=self.parse,
            dont_filter=True
        )


if __name__ == "__main__":
    from scrapy import cmdline
    cmdline.execute('scrapy crawl com.xcar.activity_car'.split())
