
import json
import uuid
import scrapy
import datetime
import random
import traceback
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {
'user-agent': 'Mozilla/5.0 (Linux; Android 10; ELE-AL00 Build/HUAWEIELE-AL00;)AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/18.0.1025 Mobile Safari/537.36 hap/1078/huawei com.huawei.fastapp/3.2.1.300 com.ddky.compzd/2.8.8 ({"packageName":"search","type":"url","extra":"{}"})',
           }


# lpush com.asos.app:start_urls "women dress"
class SsAutoSpider(RedisSpider):
    # 懂车帝
    name = 'com.ss.android.auto'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(SsAutoSpider, self).__init__(*args, **kwargs)
        self.url = 'http://m.dcdapp.com/motor/search/api/2/wap/search_content/?motor_source=quickapp&offset={}&cur_tab=6&format=json&count=10&keyword={}&headless_mode=1'
        #'http://m.dcdapp.com/motor/search/api/2/wap/search_content/?motor_source=quickapp&offset=0&cur_tab=6&format=json&count=10&keyword=%E4%B8%B0%E7%94%B0%E6%B1%BD%E8%BD%A6&headless_mode=1'

    def make_requests_from_url(self, data:json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        self.flge = True
        return scrapy.Request(
            self.url.format(0,parse.quote(query)),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1, settings.page: 0},
            callback=self.parse,
            # dont_filter=False
        )

    def parse(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        page = response.meta[settings.page]
        #bprint(response.text)
        try:
            data = json.loads(response.text).get('data')
            if len(data) == 0:
                print(query, page)
                print(response.url)
                self.flge = False
                return_no_result(app_package_id=self.name, query=query, vers=vers)
                # return
            else:
                for product in data:
                    item = QuickSpiderItem()
                    make_item(self.name, item, query, vers)
                    title_list = []
                    item_title_1 = product.get('sub_brand_name', '')
                    if item_title_1:
                        title_list.append(item_title_1)
                    item_title_2 = product.get('series_name')
                    if item_title_2:
                        title_list.append(item_title_2)
                    if len(title_list) != 0:
                        item[settings.item_title] = '-'.join(title_list)
                    else:
                        item[settings.item_title] = ''
                    item_detail_link = product.get('series_web_url', '')
                    if item_detail_link:
                        item[settings.item_detail_link] = item_detail_link
                    else:
                        item[settings.item_detail_link] = ''

                    # 图片
                    image_link = product.get('cover_url')
                    if image_link:
                        item[settings.item_image_link] = image_link
                    else:
                        item[settings.item_image_link] = ''
                    item_subtitle = product.get('level')
                    if item_subtitle:
                        item[settings.item_subtitle] = item_subtitle
                    else:
                        item[settings.item_subtitle] = ''
                    item_price = product.get('agent_price')
                    if item_price:
                        item[settings.item_price] = item_price
                    else:
                        item[settings.item_price] = ''
                    item[settings.item_type] = query.replace('汽车', '')
                    item[settings.item_class] = '汽车'
                    item[settings.item_Onelabel] = '知识'
                    item[settings.item_Twolabel] = '汽车'
                    item[settings.item_appType] = 'quick'
                    if item[settings.item_title] and item[settings.item_image_link]:
                        item[settings.item_rank] = rank
                        rank = rank + 1
                        # print(item)
                        yield item
                    # else:
                    #     return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)


        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)
        if self.flge:
            page += 10
            yield scrapy.Request(
            self.url.format(page, parse.quote(query)),
            headers=headers,
            meta={settings.vers: vers, settings.query: query, settings.rank: rank, settings.page: page},
            callback=self.parse,
            dont_filter=True
        )


if __name__ == "__main__":
    from scrapy import cmdline
    cmdline.execute('scrapy crawl com.ss.android.auto'.split())
