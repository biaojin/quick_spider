"""
添加字段
item_type ：喜剧
item_class：热剧， 热片
server_type : 泛化

"""

# 叮当快药

import json
import uuid
import scrapy
import datetime
import random
import traceback
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {
    'user-agent': 'Mozilla/5.0 (Linux; Android 10; ELE-AL00 Build/HUAWEIELE-AL00;)AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/18.0.1025 Mobile Safari/537.36 hap/1078/huawei com.huawei.fastapp/3.2.1.300 com.ddky.compzd/2.8.8 ({"packageName":"search","type":"url","extra":"{}"})',

}
url_list = {
    '战争': 'https://sou.pptv.com/category/typeid_2_cataid_144_pn_{}',
    '军旅': 'https://sou.pptv.com/category/typeid_2_cataid_139_pn_{}',
    '都市': 'https://sou.pptv.com/category/typeid_2_cataid_152_pn_{}',
    '爱情': 'https://sou.pptv.com/category/typeid_2_cataid_138_pn_{}',
    '古装': 'https://sou.pptv.com/category/typeid_2_cataid_143_pn_{}',
    '武侠': 'https://sou.pptv.com/category/typeid_2_cataid_140_pn_{}',
    '罪案': 'https://sou.pptv.com/category/typeid_2_cataid_145_pn_{}',
    '悬疑': 'https://sou.pptv.com/category/typeid_2_cataid_146_pn_{}',
    '偶像': 'https://sou.pptv.com/category/typeid_2_cataid_105_pn_{}',
    '喜剧': 'https://sou.pptv.com/category/typeid_2_cataid_149_pn_{}',
    '伦理': 'https://sou.pptv.com/category/typeid_2_cataid_147_pn_{}',
    '历史': 'https://sou.pptv.com/category/typeid_2_cataid_141_pn_{}',
    '神话': 'https://sou.pptv.com/category/typeid_2_cataid_142_pn_{}',
    '青春': 'https://sou.pptv.com/category/typeid_2_cataid_211634_pn_{}',
    '年代': 'https://sou.pptv.com/category/typeid_2_cataid_211630_pn_{}',
    '民国': 'https://sou.pptv.com/category/typeid_2_cataid_211631_pn_{}',
    '农村': 'https://sou.pptv.com/category/typeid_2_cataid_211635_pn_{}',
    '谍战': 'https://sou.pptv.com/category/typeid_2_cataid_211638_pn_{}',
    '权谋': 'https://sou.pptv.com/category/typeid_2_cataid_211633_pn_{}',
    '商战': 'https://sou.pptv.com/category/typeid_2_cataid_184_pn_{}',
    '科幻': 'https://sou.pptv.com/category/typeid_2_cataid_148_pn_{}',
    '情景': 'https://sou.pptv.com/category/typeid_2_cataid_150_pn_{}',
    '剧情': 'https://sou.pptv.com/category/typeid_2_cataid_151_pn_{}',
    '自制': 'https://sou.pptv.com/category/typeid_2_cataid_211636_pn_{}',
    '网剧': 'https://sou.pptv.com/category/typeid_2_cataid_211637_pn_{}'}


# lpush com.asos.app:start_urls "women dress"
class DushemovieBrandSpider(RedisSpider):
    name = 'com.pplive.androidphone_tv'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        self.mg = True
        super(DushemovieBrandSpider, self).__init__(*args, **kwargs)

    def make_requests_from_url(self, data: json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        url = url_list.get(query)
        print(1111111111, url)
        return scrapy.Request(
            url=url.format(1),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1, settings.page: 1, 'headers': headers},
            callback=self.parse,
            # dont_filter=False
        )


    def parse(self, response):
        query = response.meta[settings.query]
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        page = response.meta[settings.page]
        try:
            response = etree.HTML(response.text).xpath('//div[@id="listContent"]//li')
            if len(response) == 0:
                # print(query, 111111111111, mg)
                return_no_result(app_package_id=self.name, query=query, vers=vers)
            else:
                for product in response:

                    item = QuickSpiderItem()
                    make_item(self.name, item, '', vers)
                    # 名称
                    item_title = product.xpath('./div[2]/div/a/p/span[1]/text()')
                    item[settings.item_title] = item_title[0] if item_title and len(item_title) > 0 else ''
                    # 详情页链接
                    playPartId = product.xpath('./div[2]/div/a/@href')
                    item[settings.item_detail_link] = 'https:' + str(playPartId[0]) if playPartId and len(
                        playPartId) > 0 else ''
                    # 图片
                    item_image_link = product.xpath('.//div[2]/a/img/@src')
                    item[settings.item_image_link] = item_image_link[0] if item_image_link and len(
                        item_image_link) > 0 else ''
                    # 时间
                    # item[settings.item_time] = product.get('se_updateTime', '')
                    # 演员
                    item_name = product.xpath('./div[2]/div/a/div/p[1]/span/text()')
                    item[settings.item_name] = item_name[0] if item_name and len(item_name) > 0 else ''
                    # 简介
                    item_subtitle = product.xpath('./div[2]/div/a/div/p[2]/span/text()')
                    item[settings.item_subtitle] = item_subtitle[0] if item_subtitle and len(item_subtitle) > 0 else ''
                    # 评分
                    item_Praise = product.xpath('./div[2]/div/a/p/span[2]//text()')
                    item[settings.item_Praise] = ''.join(item_Praise).replace('\n', '').replace(' ',
                                                                                                '') if item_Praise and len(
                        item_Praise) > 0 else ''

                    item[settings.item_type] = query
                    item[settings.item_class] = '电视剧'
                    item[settings.server_type] = '泛化'
                    item[settings.item_Twolabel] = '电子产品 影视'
                    item[settings.item_Onelabel] = '购物 娱乐'
                    item[settings.item_appType] = 'quick'
                    if item[settings.item_title]:
                        item[settings.item_rank] = rank
                        rank = rank + 1
                        yield item
                if page < 10:
                    page += 1
                    url = url_list.get(query)
                    yield scrapy.Request(
                        url=url.format(page),
                        headers=headers,
                        meta={settings.vers: vers, settings.query: query, settings.rank: rank, settings.page: page, 'headers': headers},
                        callback=self.parse,
                        dont_filter=False
                    )

        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            if page > 100: mg = False
            ErrorSignal.main(self.name, str(traceback.format_exc()), '第{}页报错 {}'.format(page, query), e, vers)


if __name__ == "__main__":
    from scrapy import cmdline

    cmdline.execute('scrapy crawl com.pplive.androidphone_tv'.split())
