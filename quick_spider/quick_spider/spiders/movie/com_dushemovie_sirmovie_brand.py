"""
添加字段
item_type ：喜剧
item_class：热剧， 热片
server_type : 泛化

"""

# 毒舌

import json
import uuid
import scrapy
import datetime
import random
import traceback
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {
    'user-agent': 'Mozilla/5.0 (Linux; Android 10; ELE-AL00 Build/HUAWEIELE-AL00;)AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/18.0.1025 Mobile Safari/537.36 hap/1078/huawei com.huawei.fastapp/3.2.1.300 com.ddky.compzd/2.8.8 ({"packageName":"search","type":"url","extra":"{}"})',

}
movie_type = {
    '电影': 'LITE_MAIN_MOVIE',
    '电视剧': 'LITE_MAIN_TV',
}

# lpush com.asos.app:start_urls "women dress"
class DushemovieBrandSpider(RedisSpider):
    name = 'com.dushemovie.sirmovie_brand'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(DushemovieBrandSpider, self).__init__(*args, **kwargs)
        self.url = 'https://cdnapp.dushemovie.com/dsmovieapi/ssl2/weixin_movie/list_discovery_tab_movies/2?count=50&funcCode={}&startIndex=0&'

    def make_requests_from_url(self, data: json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        return scrapy.Request(
            self.url.format(movie_type.get(query)),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.item_class: query, settings.rank: 1, settings.page: 1, 'headers': headers},
            callback=self.parse,
            # dont_filter=False
        )

    def parse(self, response):
        startIndex = True
        item_class = response.meta[settings.item_class]
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        page = response.meta[settings.page]
        try:
            response = json.loads(response.text)
            if len(response.get('movieDataList', '')) == 0:
                return_no_result(app_package_id=self.name, query=item_class, vers=vers)
                startIndex = False
            else:
                for product in response.get('movieDataList'):

                    item = QuickSpiderItem()
                    make_item(self.name, item, item_class, vers)
                    # 名称
                    item[settings.item_title] = product.get('movieInfo', {}).get('title', '')
                    # 别名
                    item[settings.item_AliasCN] = product.get('movieInfo', {}).get('titleEn', '')
                    # 详情页链接
                    id = product.get('movieInfo', {}).get('id')
                    item[settings.item_detail_link] = 'https://www.dushedianying.com/movie/{}.html'.format(id)
                    # 图片
                    item[settings.item_image_link] = product.get('movieInfo', {}).get('img', '')
                    # 时间
                    item[settings.item_time] = product.get('movieInfo', {}).get('publishTime', '')
                    # 评分
                    item_Praise = product.get('movieInfo', {}).get('heatValue')
                    item[settings.item_Praise] = str(int(item_Praise) / 10) if isinstance(item_Praise, int) else ''
                    # 分类
                    item_type = product.get('movieInfo', {}).get('type', '')
                    item[settings.item_type] = ', '.join(item_type) if isinstance(item_type, list) else ''
                    # 简介
                    item[settings.item_subtitle] = product.get('movieInfo', {}).get('intro', '')
                    # 类型
                    item[settings.item_class] = item_class
                    # 演员
                    name_list = []
                    item_name1 = product.get('actorBasicInfoList')  # 演员
                    item_name2 = product.get('directorBasicInfoList')  # 导演
                    if isinstance(item_name1, list):
                        for name in item_name1:
                            name_list.append(name.get('name', ''))
                    if len(name_list) == 0:
                        if isinstance(item_name2, list):
                            for name in item_name2:
                                name_list.append(name.get('name', ''))
                            item[settings.item_name] = ', '.join(name_list)
                        else:item[settings.item_name] = ''
                    else:item[settings.item_name] = ', '.join(name_list)

                    item[settings.item_Onelabel] = '娱乐'
                    item[settings.item_Twolabel] = '影视'
                    item[settings.server_type] = '泛化'
                    item[settings.item_appType] = 'quick'
                    if item[settings.item_title] and item[settings.item_subtitle]:
                        item[settings.item_rank] = rank
                        rank = rank + 1
                        yield item
                # else: return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)
                if page:
                    page += 1
                    yield scrapy.Request(
                        url='https://cdnapp.dushemovie.com/dsmovieapi/ssl2/weixin_movie/list_discovery_tab_movies/2?count=50&funcCode={}&startIndex={}&'.format(movie_type.get(item_class), page),
                        headers=headers,
                        meta={settings.vers: vers, settings.item_class: item_class, settings.rank: rank, settings.page: page, 'headers': headers},
                        callback=self.parse,
                        dont_filter=False
                    )

        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            if page > 100: startIndex = False
            ErrorSignal.main(self.name, str(traceback.format_exc()), '第{}页报错 {}'.format(page, item_class), e, vers)


if __name__ == "__main__":
    from scrapy import cmdline

    cmdline.execute('scrapy crawl com.dushemovie.sirmovie_brand'.split())
