# 叮当快药

import json
import uuid
import scrapy
import datetime
import random
import traceback
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {
    'user-agent': 'Mozilla/5.0 (Linux; Android 10; ELE-AL00 Build/HUAWEIELE-AL00;)AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/18.0.1025 Mobile Safari/537.36 hap/1078/huawei com.huawei.fastapp/3.2.1.300 com.ddky.compzd/2.8.8 ({"packageName":"search","type":"url","extra":"{}"})',

}

movie_type={
    '热映':'movieOnInfoList?token=',
    '即将上映':'comingList?ci=1&token=&limit=10',
}

# lpush com.asos.app:start_urls "women dress"
class SanKuaiSpider(RedisSpider):
    name = 'com.sankuai.movie_brand'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(SanKuaiSpider, self).__init__(*args, **kwargs)
        self.url = 'https://m.maoyan.com/ajax/{}&optimus_uuid=C96240B09B5F11EBBF8D336B4E14C9A65DA6D03EF9CF42B6B2EBF96463E6F32C&optimus_risk_level=71&optimus_code=10'

    def make_requests_from_url(self, data: json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        return scrapy.Request(
            self.url.format(movie_type.get(query)),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1,
                  settings.page: 1, 'headers': headers},
            callback=self.parse_linkurl,
            #dont_filter=False
        )

    def parse_linkurl(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        try:
            data = json.loads(response.text)
            link_id = data.get('movieIds')
            for i in link_id:
                    link_url = 'https://api.maoyan.com/mmdb/movie/v5/{}.json'.format(i)
                    rank += 1
                    yield scrapy.Request(
                        link_url,
                        headers=headers,
                        meta={settings.vers: vers, settings.query: query, settings.rank: rank, settings.page: 1, 'headers': headers},
                        callback=self.parse,
                        dont_filter=True
                    )

        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)

    def parse(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        try:
            data = json.loads(response.text)
            if len(data.get('data', '')) == 0 or '"error":{"code":400' in response.text:
                return_no_result(app_package_id=self.name, query=query, vers=vers)
            else:
                product = data.get('data', {}).get('movie')
                item = QuickSpiderItem()

                make_item(self.name, item, '', vers)
                item[settings.item_title] = product.get('nm', '')

                id = product.get('id')
                item[settings.item_detail_link] = 'https://m.maoyan.com/asgard/movie/{}?&channelId=1000207&cityId=1&$from=hwfastapp#'.format(id)
                # 图片
                image_link = product.get('img', '')
                if image_link:
                    item[settings.item_image_link] = image_link.replace('w.h/', '')
                else:
                    item[settings.item_image_link] = ''
                # 电影简介
                item_subtitle = product.get('dra', '')
                if item_subtitle:
                    item[settings.item_subtitle] = item_subtitle
                else:
                    item[settings.item_subtitle] = ''
                # 电影时间
                item_time = product.get('rt', '')
                if item_time:
                    item[settings.item_time] = item_time
                else:
                    item[settings.item_time] = ''
                # 电影别名
                item_AliasCN = product.get('enm', '')
                if item_AliasCN:
                    item[settings.item_AliasCN] = item_AliasCN
                else:
                    item[settings.item_AliasCN] = ''
                # 电影分类
                item_type = product.get('cat', '')
                if item_type:
                    item[settings.item_type] = item_type
                else:
                    item[settings.item_type] = ''
                # 电影演员
                item_name = product.get('star', '')
                if item_name:
                    item[settings.item_name] = item_name
                else:
                    item[settings.item_name] = ''
                # 电影评分
                item_Praise = product.get('sc', '')
                if item_Praise:
                    item[settings.item_Praise] = item_Praise
                else:
                    item[settings.item_Praise] = ''
                # 电影时长
                itemDuration = product.get('dur', '')
                if itemDuration:
                    item[settings.item_duration] = itemDuration
                else:
                    item[settings.item_duration] = ''

                item_time = product.get('pubDate')
                if item_time:
                    item[settings.item_time] = item_time
                else:
                    item[settings.item_time] = ''
                item_adder = product.get('src')
                if item_adder:
                    item[settings.item_adder] = item_adder
                else:
                    item[settings.item_adder] = ''
                item[settings.item_class] = '电影 {}'.format(query)
                item[settings.server_type] = '泛化'
                item[settings.item_Onelabel] = '娱乐'
                item[settings.item_Twolabel] = '影视'
                item[settings.item_appType] = 'quick'
                if item[settings.item_title] and item[settings.item_image_link]:
                    item[settings.item_rank] = rank
                    # rank = rank + 1
                    yield item
                # else: return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)


        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)


if __name__ == "__main__":
    from scrapy import cmdline

    cmdline.execute('scrapy crawl com.sankuai.movie_brand'.split())
