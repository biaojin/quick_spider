"""
添加字段
item_type ：喜剧
item_class：热剧， 热片
server_type : 泛化

"""

# 叮当快药

import json
import uuid
import scrapy
import datetime
import random
import traceback
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {
    'user-agent': 'Mozilla/5.0 (Linux; Android 10; ELE-AL00 Build/HUAWEIELE-AL00;)AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/18.0.1025 Mobile Safari/537.36 hap/1078/huawei com.huawei.fastapp/3.2.1.300 com.ddky.compzd/2.8.8 ({"packageName":"search","type":"url","extra":"{}"})',

}
tv_id = {
    '甜蜜互宠': '14',
    '虐恋情深': '15',
    '青涩校园': '16',
    '仙侠玄幻': '17',
    '都市职场': '18',
    '古装': '19',
    '快意江湖': '20',
    '偶像': '147',
    '悬疑推理': '21',
    '家长里短': '22',
    '芒果出品': '23',
    '轻松搞笑': '24',
    '铁血战争': '25',
    '其他': '26',
}


# lpush com.asos.app:start_urls "women dress"
class DushemovieBrandSpider(RedisSpider):
    name = 'com.hunantv.imgo.activity_tv'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        self.mg = True
        super(DushemovieBrandSpider, self).__init__(*args, **kwargs)
        self.url = 'https://pianku.api.mgtv.com/rider/list/msite/v2?area=10&pc=12&kind={}&edition=all&sort=c1&channelId=2&pn={}'

    def make_requests_from_url(self, data: json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        return scrapy.Request(
            self.url.format(tv_id.get(query), 1),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1,
                  settings.page: 1, 'headers': headers},
            callback=self.parse,
            # dont_filter=False
        )

    def parse(self, response):
        mg = True
        query = response.meta[settings.query]
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        page = response.meta[settings.page]
        try:
            response = json.loads(response.text)
            if len(response.get('data', {}).get('hitDocs', '')) == 0:
                print(query, 111111111111, mg)
                return_no_result(app_package_id=self.name, query=query, vers=vers)
                mg = False
                print(query, 2222222222222,mg)
            else:
                for product in response.get('data', {}).get('hitDocs'):

                    item = QuickSpiderItem()
                    make_item(self.name, item, '', vers)
                    # 名称
                    item[settings.item_title] = product.get('title', '')
                    # 详情页链接
                    id = product.get('clipId', '')
                    playPartId = product.get('playPartId', '')
                    if id and playPartId:
                        item[settings.item_detail_link] = 'https://www.mgtv.com/b/{}/{}.html'.format(id, playPartId)
                    elif id and len(id) != 0:
                        item[settings.item_detail_link] = 'https://www.mgtv.com/b/{}.html'.format(id, playPartId)
                    else:
                        item[settings.item_detail_link] = ''
                    # 图片
                    item[settings.item_image_link] = product.get('img', '')
                    # 时间
                    item[settings.item_time] = product.get('se_updateTime', '')
                    # 演员
                    item[settings.item_name] = product.get('subtitle', '')

                    item[settings.item_type] = query
                    item[settings.item_class] = '电视剧'
                    item[settings.server_type] = '泛化'
                    item[settings.item_Twolabel] = '电子产品 影视'
                    item[settings.item_Onelabel] = '购物 娱乐'
                    item[settings.item_appType] = 'quick'
                    if item[settings.item_title]:
                        item[settings.item_rank] = rank
                        rank = rank + 1
                        yield item
                # else: return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)
                if page:
                    page += 1
                    print(11111111, query, rank)
                    yield scrapy.Request(
                        url='https://pianku.api.mgtv.com/rider/list/msite/v2?area=a1&pc=12&kind={}&edition=all&sort=c2&channelId=2&pn={}'.format(tv_id.get(query), page),
                        headers=headers,
                        meta={settings.vers: vers, settings.query: query, settings.rank: rank, settings.page: page, 'headers': headers},
                        callback=self.parse,
                        dont_filter=False
                    )

        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            if page > 100: mg = False
            ErrorSignal.main(self.name, str(traceback.format_exc()), '第{}页报错 {}'.format(page, query), e, vers)


if __name__ == "__main__":
    from scrapy import cmdline

    cmdline.execute('scrapy crawl com.hunantv.imgo.activity_tv'.split())
