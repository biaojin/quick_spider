# 叮当快药

import json
import uuid
import scrapy
import datetime
import random
import traceback
import requests
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {
    'user-agent': 'Mozilla/5.0 (Linux; Android 10; ELE-AL00 Build/HUAWEIELE-AL00;)AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/18.0.1025 Mobile Safari/537.36 hap/1078/huawei com.huawei.fastapp/3.2.1.300 com.ddky.compzd/2.8.8 ({"packageName":"search","type":"url","extra":"{}"})',

}

query_id = {
    '华语': '2001000',
    '日韩': '2002000',
    '欧美': '2003000',
    '其他': '2004000',
    '恐怖': '100002602',
    '剧情': '1001000',
    '喜剧': '1002000',
    '动作': '1003000',
    '奇幻': '1004000',
    '犯罪': '1005000',
    '科幻': '1006000',
    '惊悚': '1007000',
    '动画': '1017000',
    '冒险': '1008000',
    '悬疑': '1009000',
    '战争': '1010000',
    '音乐': '1011000',
    '传记': '1012000',
    '爱情': '1013000',
    '家庭': '1014000',
    '历史': '1016000',
    '古装': '1024000',
    '综艺': '1031000',
    '纪录片': '1043000',
    '美剧': '1065000',
    '英剧': '1066000',
    '韩剧': '1067000',
    '日剧': '1068000',
    '国产剧': '1069000',
    '港剧': '1070000',

}


# lpush com.asos.app:start_urls "women dress"
class DushemovieSpider(RedisSpider):
    name = 'com.dushemovie.sirmovie'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(DushemovieSpider, self).__init__(*args, **kwargs)
        self.url = 'http://www.dushedianying.com/movie/?cid={}&p={}'

    def make_requests_from_url(self, data: json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        return scrapy.Request(
            self.url.format(query_id.get(query), 1),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1,
                  settings.page: 1, 'headers': headers},
            callback=self.parse,
            # dont_filter=False
        )

    def parse(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        page = response.meta[settings.page]
        try:
            if response.text:
                response = etree.HTML(response.text).xpath('//div[@class="movieitem"]')
                if len(response) == 0:
                    return_no_result(app_package_id=self.name, query=query, vers=vers)
                else:
                    for product in response:

                        item = QuickSpiderItem()
                        make_item(self.name, item, query, vers)

                        # 详情链接
                        item_detail_link = product.xpath('./a/@href')
                        if len(item_detail_link) == 0:
                            item[settings.item_detail_link] = ''
                        else:
                            item[settings.item_detail_link] = 'http://www.dushedianying.com' + item_detail_link[0]
                        # 标题
                        item[settings.item_title] = product.xpath('./a/h4/text()')[0].strip()

                        link_data = self.get_link(item[settings.item_detail_link])
                        # 摘要
                        item[settings.item_subtitle] = link_data.get('item_subtitle', '')
                        # 评分
                        item[settings.item_Praise] = link_data.get('item_Praise', '')
                        # 演员
                        item[settings.item_name] = link_data.get('item_name', '')
                        # 类型
                        item_type = link_data.get('item_type', [])
                        if str(query) in item_type:
                            item[settings.item_type] = ', '.join(item_type)
                        else:
                            item_type.append(query)
                            item[settings.item_type] = ', '.join(item_type)
                        # 上映时间
                        item[settings.item_time] = link_data.get('item_time', '')
                        # 图片
                        item[settings.item_image_link] = link_data.get('item_image_link', '')

                        item[settings.item_class] = '电影'
                        item[settings.item_Onelabel] = '娱乐'
                        item[settings.item_Twolabel] = '影视'
                        item[settings.item_appType] = 'quick'
                        item[settings.server_type] = '泛化1'
                        if item[settings.item_title] and item[settings.item_image_link] and item[
                            settings.item_detail_link]:
                            item[settings.item_rank] = rank
                            rank = rank + 1
                            yield item
                    # else: return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)
                    if page < 10:
                        page = page + 1
                        yield scrapy.Request(
                            self.url.format(query_id.get(query), page),
                            headers=headers,
                            meta={settings.vers: vers, settings.query: query, settings.rank: rank, settings.page: page,
                                  'headers': headers},
                            callback=self.parse,
                            dont_filter=False
                        )
            else:
                return_no_result(app_package_id=self.name, query=query, vers=vers)


        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)

    def get_link(self, url):
        try:
            item = dict()
            response = requests.get(url=url, headers=headers).text
            if len(response) == 0:
                return {}
            else:
                data = etree.HTML(response)
                item_subtitle = data.xpath('/html/body/div[3]/div/div[2]/p/text()')
                item_subtitle = item_subtitle[0] if len(item_subtitle) >= 1 else ''
                item_Praise = data.xpath('//div[@class="rating"]/text()')
                item_Praise = item_Praise[0] if len(item_Praise) >= 1 else ''
                item_name = data.xpath('/html/body/div[3]/div/div[2]/dl[2]/dd/text()')
                item_name = item_name[0] if len(item_name) >= 1 else ''
                item_type = data.xpath('/html/body/div[3]/div/div[2]/dl[3]/dd/text()')
                item_type = item_type[0] if len(item_type) >= 1 else ''
                item_time = data.xpath('/html/body/div[3]/div/div[2]/dl[4]/dd/text()')
                item_time = item_time[0] if len(item_time) >= 1 else ''
                item_image_link = data.xpath('/html/body/div[3]/div/div[1]/img/@src')
                item_image_link = item_image_link[0] if len(item_image_link) >= 1 else ''

                item['item_subtitle'] = item_subtitle.strip().replace('“', '').replace('”', '')
                item['item_Praise'] = item_Praise.replace('\r\n', '').strip()
                item['item_name'] = item_name.replace('\r\n', '').strip()
                item['item_time'] = item_time.replace('\r\n', '').strip()
                item['item_time'] = item_time.replace('\r\n', '').strip()
                item['item_image_link'] = item_image_link.strip()
                item['item_type'] = item_type.replace('\r\n', '').strip().split('/')
                return item
        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))


if __name__ == "__main__":
    from scrapy import cmdline

    cmdline.execute('scrapy crawl com.dushemovie.sirmovie'.split())
