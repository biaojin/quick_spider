"""
添加字段
item_type ：喜剧
item_class：热剧， 热片
server_type : 泛化

"""

# 叮当快药

import json
import uuid
import scrapy
import datetime
import random
import requests
import traceback

from scraper_api import ScraperAPIClient
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36',
    'x-requested-with': 'XMLHttpRequest'}
query_list = ['武侠', '警匪', '犯罪', '科幻', '战争', '恐怖', '惊悚', '纪录片', '西部', '戏曲', '歌舞', '奇幻', '冒险', '悬疑', '历史', '动作', '传记',
              '动画', '儿童', '喜剧', '爱情', '剧情', '运动', '短片', '优酷出品']


# lpush com.asos.app:start_urls "women dress"
class DushemovieBrandSpider(RedisSpider):
    name = 'com.youku.phone_movie1'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(DushemovieBrandSpider, self).__init__(*args, **kwargs)
        self.url = 'https://www.youku.com/category/data?c=96&a=&g={}&type=show&p={}'

    def make_requests_from_url(self, data: json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        headers['referer'] = 'https://www.youku.com/category/show/c_96_g_{}.html?theme=dark'.format(parse.quote(query))
        return scrapy.Request(
            self.url.format(parse.quote(query), 1),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1, settings.page: 1, 'headers': headers},
            callback=self.parse,
            # dont_filter=False
        )

    def get_item_name(self, url):
        try:
            client = ScraperAPIClient('cb43bcd34c88670a2a5b81db1bd16a85')
            if url.startswith('http'):
                rsp = client.get(url=url, headers=headers).text
                data = re.findall('true;window.__INITIAL_DATA__ =(.*?);</script>', rsp, re.S)
                name_list = []
                if data and len(data) > 0:
                    con = json.loads(data[0]).get('data', {}).get('data', {}).get('nodes', [{}])[0].get('nodes', [{}])[0].get('nodes')
                    if con and len(con) > 0:
                        for i in con[1:-1]:
                            name = i.get('data', {}).get('title', '')
                            if name and name is not None:
                                name_list.append(name)
                return name_list
        except Exception as e:
            print(e)

    def parse(self, response):
        query = response.meta[settings.query]
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        page = response.meta[settings.page]
        headers = response.meta['headers']
        try:
            print(111111111111, headers)

            response = json.loads(response.text)
            if len(response.get('data', {}).get('categoryVideos', '')) == 0:
                return_no_result(app_package_id=self.name, query=query, vers=vers)
            else:
                for product in response.get('data', {}).get('categoryVideos'):

                    item = QuickSpiderItem()
                    make_item(self.name, item, '', vers)
                    # 名称
                    item[settings.item_title] = product.get('title', '')
                    # 简介
                    item[settings.item_subtitle] = product.get('subTitle', '')
                    # 详情页链接
                    playPartId = product.get('videoLink', '')
                    if playPartId and playPartId.startswith('//'):
                        item[settings.item_detail_link] = 'https:{}'.format(playPartId)
                    else:
                        item[settings.item_detail_link] = ''
                    # 图片
                    item_image_link = product.get('img', '')
                    if item_image_link and item_image_link.startswith('//'):
                        item[settings.item_image_link] = 'https:{}'.format(item_image_link)
                    else:
                        item[settings.item_image_link] = ''
                    # 时间
                    # item[settings.item_time] = product.get('se_updateTime', '')
                    # 演员
                    item[settings.item_name] = ', '.join(self.get_item_name(item[settings.item_detail_link]))

                    item[settings.item_type] = query
                    item[settings.item_class] = '电影'
                    item[settings.server_type] = '泛化'
                    item[settings.item_Twolabel] = '电子产品 影视'
                    item[settings.item_Onelabel] = '购物 娱乐'
                    item[settings.item_appType] = 'quick'
                    print(1111111111,item[settings.item_title])
                    if item[settings.item_title]:
                        item[settings.item_rank] = rank
                        rank = rank + 1
                        yield item
                # else: return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)
                if page < 10:
                    page += 1
                    print(11111111, query, rank)
                    yield scrapy.Request(
                        self.url.format(parse.quote(query), page),
                        headers=headers,
                        meta={settings.vers:vers, settings.query: query, settings.rank: rank,
                              settings.page: page, 'headers': headers},
                        callback=self.parse,
                        dont_filter=False
                    )

        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))

            if page > 100: mg = False
            ErrorSignal.main(self.name, str(traceback.format_exc()), '第{}页报错 {}'.format(page, query), e, vers)



if __name__ == "__main__":
    from scrapy import cmdline

    cmdline.execute('scrapy crawl com.youku.phone_movie1'.split())
