# 叮当快药

import json
import uuid
import scrapy
import datetime
import random
import traceback
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {
'user-agent': 'Mozilla/5.0 (Linux; Android 10; ELE-AL00 Build/HUAWEIELE-AL00;)AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/18.0.1025 Mobile Safari/537.36 hap/1078/huawei com.huawei.fastapp/3.2.1.300 com.ddky.compzd/2.8.8 ({"packageName":"search","type":"url","extra":"{}"})',
           }


# lpush com.asos.app:start_urls "women dress"
class MgTvSpider(RedisSpider):
    name = 'com.hunantv.imgo.activity'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(MgTvSpider, self).__init__(*args, **kwargs)
        self.url = 'https://mobileso.bz.mgtv.com/applet/search/v1?q={}&_support=10000000&pc=10&pn=1'

    def make_requests_from_url(self, data:json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        return scrapy.Request(
            self.url.format(query),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1, settings.page: 1},
            callback=self.parse,
            # dont_filter=False
        )

    def parse(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        #bprint(response.text)
        try:
            data = json.loads(response.text).get('data')
            if len(data.get('contents')) == 0:
                return_no_result(app_package_id=self.name, query=query, vers=vers)
            else:
                for products in data.get('contents'):
                    item = QuickSpiderItem()
                    make_item(self.name, item, query, vers)
                    try:
                        product = products.get('data')[0]
                    except:
                        print(6666666666666666666666666, products)
                        return
                    item[settings.item_title] = product.get('title', '')
                    vid = product.get('vid', '')
                    item[settings.item_detail_link] = 'https://www.mgtv.com/s/{}.html?fpa=se&lastp=so_result'.format(vid)
                    # 图片
                    image_link = product.get('img')
                    if image_link:
                        item[settings.item_image_link] = image_link
                    else:
                        item[settings.item_image_link] = ''
                    item_subtitle = product.get('desc')
                    if item_subtitle:
                            item[settings.item_subtitle] = item_subtitle[0]
                    else:
                            item[settings.item_subtitle] = ''
                    item_duration = product.get('rightBottomCorner')
                    if item_duration:
                        item[settings.item_duration] = item_duration
                    else:
                        item[settings.item_duration] = ''
                    item[settings.item_Twolabel] = '电子产品 影视'
                    item[settings.item_Onelabel] = '购物 娱乐'
                    item[settings.item_appType] = 'quick'
                    if item[settings.item_title] and item[settings.item_image_link]:
                        item[settings.item_rank] = rank
                        rank = rank + 1
                        # print(item)
                        yield item
                    # else:
                    #     return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)


        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)


if __name__ == "__main__":
    from scrapy import cmdline
    cmdline.execute('scrapy crawl com.hunantv.imgo.activity'.split())
