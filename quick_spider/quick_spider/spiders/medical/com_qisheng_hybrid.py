# 39健康

import json
import uuid
import scrapy
import datetime
import random
import traceback
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
from scrapy.utils.project import get_project_settings

headers = {
    'user-agent': 'Mozilla/5.0 (Linux; Android 10; ELE-AL00 Build/HUAWEIELE-AL00;)AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/18.0.1025 Mobile Safari/537.36 hap/1078/huawei com.huawei.fastapp/3.2.1.300 com.ddky.compzd/2.8.8 ({"packageName":"search","type":"url","extra":"{}"})',
    'Content-Type': 'application/json',
    }
disease_dict = {
    '症状': 'symptom',
    '疾病': 'disease',
}


# lpush com.asos.app:start_urls "women dress"
class Qishen39Spider(RedisSpider):
    name = 'com.qisheng.hybrid'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(Qishen39Spider, self).__init__(*args, **kwargs)
        self.url = 'http://so-api.39.net/disease/diseasedrug.ashx?PageSize=1&words={}&PageIndex=1'

    def make_requests_from_url(self, data: json):
        query = json.loads(data).get('query')
        # query = '糖尿病'
        return_all_query(self.name, data)

        return scrapy.Request(
            self.url.format(parse.quote(query)),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1, settings.page: 1, 'headers': headers},
            callback=self.parse,
            dont_filter=True

        )

    def parse(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        try:
            # print(response.text)
            data = json.loads(response.text).get('data')
            if 'diseasetype' not in response.text or len(data.get('items')) == 0:
                url = 'https://interface.39.net/cmswebapi/api/fastapp/v2/getarticlesnew?pageIndex=1&pageSize=20&word={}'
                yield scrapy.Request(
                    url.format(parse.quote(query)),
                    headers=headers,
                    meta={settings.vers: vers, settings.query: query, settings.rank: 1, settings.page: 1},
                    callback=self.parse_content,
                    dont_filter=True
                )
            else:
                id = data.get('items')[0].get('_id')
                diseasetype = disease_dict.get(data.get('items')[0].get('diseasetype'))
                introducecn = data.get('items')[0].get('introducecn')
                if id and diseasetype:
                    url = 'https://interface.39.net/openapi/jbk/getdetailbyid?app_key=mixapp&prefix={}&sign=AC09B828162E916EA87181CF5A57C870&format=false&id={}'
                    yield scrapy.Request(
                        url.format(diseasetype, id),
                        headers=headers,
                        meta={settings.vers: vers, settings.query: query, settings.rank: 1,
                              settings.page: 1, 'introducecn': introducecn, 'diseasetype': diseasetype},
                        callback=self.parse_data,
                        dont_filter=True
                    )
                else:
                    raise Exception('%s:需要查看类型' % query)
        except Exception as e:
            print(e)
            traceback.print_exc()
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)

    def parse_data(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        diseasetype = response.meta['diseasetype']
        # print(response.text)
        try:
            data = json.loads(response.text).get('results')
            item = QuickSpiderItem()
            make_item(self.name, item, query, vers)
            item[settings.item_title] = data.get('NameCN', '')
            _id = data.get('_id')
            item[settings.item_detail_link] = 'https://hapjs.org/app/com.qisheng.hybrid/view/disease/detail?id={}&type={}&typeNum=0&g_ref_s=share.85a6b251146a98e53e948ef62f241078'.format(_id, diseasetype)
            # 别名
            AliasCN = data.get("AliasCN")
            if AliasCN:
                item[settings.item_AliasCN] = AliasCN
            else:
                item[settings.item_AliasCN] = ''
            # 基础知识 list
            DiseaseTagName = data.get('DiseaseTagName')
            if DiseaseTagName:
                item[settings.item_DiseaseTagName] = json.dumps(DiseaseTagName)
            else:
                item[settings.item_DiseaseTagName] = ''
            item[settings.item_subtitle] = response.meta.get('introducecn')
            # 发病部位
            RelateBody = data.get('RelateBody')
            if RelateBody:
                item[settings.item_RelateBody] = RelateBody
            else:
                item[settings.item_RelateBody] = ''
            # 图片
            item[settings.item_image_link] = ''
            # 并发症
            DiseaseList = data.get('D_RelateDiseaseList')
            if DiseaseList:
                disease_list = []
                for i in DiseaseList:
                    disease_list.append(i.get('NameCN'))
                item[settings.item_DiseaseList] = json.dumps(disease_list)
            else:
                item[settings.item_DiseaseList] = ''
            # 症状
            Symptom = data.get('Symptom')
            if Symptom:
                Symptom = self.xml_data(Symptom)
                item[settings.item_Symptom] = Symptom
            else:
                item[settings.item_Symptom] = ''
            # 病因
            Cause = data.get('Cause')
            if Cause:
                Cause = self.xml_data(Cause)
                item[settings.item_Cause] = Cause
            else:
                item[settings.item_Cause] = ''
                # 治疗
            XyTreated = data.get('XyTreated')
            if XyTreated:
                XyTreated = self.xml_data(XyTreated)
                item[settings.item_XyTreated] = XyTreated
            else:
                item[settings.item_XyTreated] = ''
            item[settings.item_Twolabel] = '医疗健康 药品'
            item[settings.item_Onelabel] = '生活 购物'
            item[settings.item_appType] = 'quick'
            if item[settings.item_title] and item[settings.item_detail_link]:
                item[settings.item_rank] = rank
                rank = rank + 1
                # print(item)
                yield item
            else:
                return_no_result(app_package_id=self.name, query=query, vers=vers)
        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)

    def parse_content(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        # print(response.text)
        try:
            data = json.loads(response.text).get('data', {})
            if len(data.get('items', '')) == 0:
                return_no_result(app_package_id=self.name, query=query, vers=vers)
            else:
                for con in data.get('items', []):
                    item = QuickSpiderItem()
                    make_item(self.name, item, query, vers)
                    # 标题
                    item[settings.item_title] = con.get('Title', '')
                    # 详情页链接
                    link_id = con.get('Id', '')
                    if not link_id and len(link_id) == 0:
                        item[settings.item_detail_link] = ''
                    else:
                        item[settings.item_detail_link] = 'https://hapjs.org/app/com.qisheng.hybrid/view/reading/article/detail?id={}&g_ref_s=share.a741dd0fb71e915cc827ce66598cd64f'.format(link_id)
                    # 摘要
                    item[settings.item_subtitle] = con.get('Summary', '')
                    # 图片
                    item[settings.item_image_link] = con.get('Thumbnail', '')
                    item[settings.item_Twolabel] = '医疗健康 药品'
                    item[settings.item_Onelabel] = '生活 购物'
                    item[settings.item_appType] = 'quick'
                    if item[settings.item_title] and item[settings.item_detail_link]:
                        item[settings.item_rank] = rank
                        rank = rank + 1
                        # print(item)
                        yield item
                    else:
                        return_no_result(app_package_id=self.name, query=query, vers=vers)
        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)

    def xml_data(self, text):
        html = etree.HTML(text)
        xml_list = []
        for a in html.xpath('//p'):
            xml_list.append(''.join(a.xpath('string(.)')).replace('\u3000', ''))
            if len(''.join(xml_list)) > 300:
                break
        return ''.join(xml_list)


if __name__ == "__main__":
    from scrapy import cmdline

    cmdline.execute('scrapy crawl com.qisheng.hybrid'.split())
