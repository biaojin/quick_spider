# 叮当快药

import json
import uuid
import scrapy
import datetime
import random
import traceback
from scrapy_redis.spiders import RedisSpider
from quick_spider.tools.error_signal import ErrorSignal
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.spider_tools import *
from quick_spider import settings
from urllib.parse import unquote_plus
from scrapy import FormRequest
from lxml.html import etree
from urllib import parse
import hashlib
from scrapy.utils.project import get_project_settings

headers = {'Accept-Language': 'zh-CN',
'Content-Type': 'text/plain',
'user-agent': 'Mozilla/5.0 (Linux; Android 10; ELE-AL00 Build/HUAWEIELE-AL00;)AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/18.0.1025 Mobile Safari/537.36 hap/1078/huawei com.huawei.fastapp/3.2.1.300 com.ddky.compzd/2.8.8 ({"packageName":"search","type":"url","extra":"{}"})',
'Host': 'api.ddky.com',
'Connection': 'Keep-Alive',
'Accept-Encoding': 'gzip'
           }


# lpush com.asos.app:start_urls "women dress"
class DdsySpider(RedisSpider):
    name = 'com.ddsy.songyao'
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        self.img_list = []
        # domain = kwargs.pop('domain', '')
        # self.allowed_domains = filter(None, domain.split(','))
        super(DdsySpider, self).__init__(*args, **kwargs)
        self.url = 'https://api.ddky.com/cms/rest.htm?sign={}&city={}&idfa=eaedcbd2-df7f-e177-7eed-cffd4d77d27d&lat={}&lng={}&method=ddky.cms.all.search.spells.blend&orderTypeId=0&pageNo=1&pageSize=20&plat=xmZd&platform=xmZd&searchPanel=1&searchType=o2o&shopId=30853&suite=1&t={}&v=1.0&versionName=4.9.0&wd={}'

    def make_requests_from_url(self, data:json):
        query = json.loads(data).get('query')
        return_all_query(self.name, data)
        time_ = time.strftime('%Y-%M-%d %H:%M:%S', time.localtime())
        url_params = 'city={}&idfa=eaedcbd2-df7f-e177-7eed-cffd4d77d27d&lat={}&lng={}&method=ddky.cms.all.search.spells.blend&orderTypeId=0&pageNo=1&pageSize=20&plat=xmZd&platform=xmZd&searchPanel=1&searchType=o2o&shopId=30853&suite=1&t={}&v=1.0&versionName=4.9.0&wd={}'.format(
            parse.quote('东莞市'),'23.025662','113.754366', parse.quote(time_), parse.quote(query))
        # method参数
        method = re.compile(r'method=([^&]+)').search(url_params).groups()[0]
        # 构造出来的p参数
        p = unquote_plus(url_params.replace('=', '').replace('&', ''))
        # r参数的值固定
        r = '6C57AB91A1308E26B797F4CD382AC79D'
        # 计算sign
        m = hashlib.md5()
        m.update(str(method + p + r).encode('utf-8'))
        sign = m.hexdigest().upper()
        print(self.url.format(sign, parse.quote('东莞市'), '23.025662','113.754366', parse.quote(time_),parse.quote(query)))
        return scrapy.Request(
            self.url.format(sign, parse.quote('东莞市'), '23.025662','113.754366', parse.quote(time_),parse.quote(query)),
            headers=headers,
            meta={settings.vers: json.loads(data).get('vers'), settings.query: query, settings.rank: 1, settings.page: 1,  'headers': headers},
            callback=self.parse,
            # dont_filter=False
        )

    def parse(self, response):
        query = response.meta['query']
        vers = response.meta[settings.vers]
        rank = response.meta[settings.rank]
        print(response.text)
        try:
            data = json.loads(response.text).get('result', {})
            if len(data.get('productList', '')) == 0:
                return_no_result(app_package_id=self.name, query=query, vers=vers)
            elif data.get('msg') == 'sign参数错误':
                pass
            else:
                for product in data.get('productList'):
                    item = QuickSpiderItem()

                    make_item(self.name, item, query, vers)
                    item[settings.item_title] = product.get('name', '')
                    shopId = product.get('shopId', '')
                    skuId = product.get('skuId')
                    item[settings.item_detail_link] = ' http://m.ddky.com/detail.html?shopId={}&skuId={}&suite=1&footerStatu=2&position=8202'.format(shopId,  skuId)
                    # 图片
                    image_link = product.get('imgUrl')
                    if image_link:
                        item[settings.item_image_link] = image_link
                    else:
                        item[settings.item_image_link] = ''
                    item_subtitle = product.get('productDescription')
                    if item_subtitle:
                        item[settings.item_subtitle] = item_subtitle
                    else:
                        item[settings.item_subtitle] = ''
                    # 规格
                    Symptom = product.get('productSpecifications')
                    if Symptom:
                        item[settings.item_Symptom] = Symptom
                    else:
                        item[settings.item_Symptom] = ''
                    item_price = product.get('productMarketPrice')
                    if item_price:
                        item[settings.item_price] = item_price
                    else:
                        item[settings.item_price] = ''
                    pro_list = []
                    promotionTipList = product.get('promotionTipList')
                    if len(promotionTipList) != 0:
                        for pro in promotionTipList:
                            pro_list.append(pro.get('promotionTip'))
                    item[settings.item_other] = ','.join(pro_list)
                    item_time = product.get('tab')
                    if item_time:
                        item[settings.item_time] = item_time
                    else:
                        item[settings.item_time] = ''
                    item_adder = product.get('shopName')
                    if item_adder:
                        item[settings.item_adder] = item_adder
                    else:
                        item[settings.item_adder] = ''
                    item[settings.item_Twolabel] = '医疗健康 药品'
                    item[settings.item_Onelabel] = '生活 购物'
                    item[settings.item_appType] = 'quick'
                    if item[settings.item_title] and item[settings.item_image_link]:
                        item[settings.item_rank] = rank
                        rank = rank + 1
                        # print(item)
                        yield item
                    # else:
                    #     return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)


        except Exception as e:
            print(self.name + ":" + str(e))
            traceback.print_exc()
            print(self.name + ' error -- ' + str(e))
            ErrorSignal.main(self.name, str(traceback.format_exc()), query, e, vers)


if __name__ == "__main__":
    from scrapy import cmdline
    cmdline.execute('scrapy crawl com.ddsy.songyao'.split())
