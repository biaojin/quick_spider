# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json, re
from quick_spider import settings
from scrapy.exceptions import DropItem
import requests
import redis
from quick_spider.configs.AppInfo import app_info_dict
from twisted.internet import reactor, defer
from quick_spider.items import QuickSpiderItem
from quick_spider.tools.error_signal import ErrorSignal
from twisted.enterprise import adbapi
import pymysql
import traceback


class HotSpiderPipeline(object):
    # 替换所有特殊字符
    def clear_spaces(self, subtitle):
        subtitle = str(subtitle)
        subtitle1 = re.sub(r"<(.*?)>|/\*(.*?)\*/|amp;|\\[a-z][\d+]{3,}[a-z]|br|\\|&zwj;|\\u[\d+]|\n|\r", ' ', subtitle)
        subtitle2 = re.sub('&#39;|&#039;|&lsquo;|&rsquo;|&ndash;|&apos;|nbsp|nbsp;|\u3000', "'", subtitle1)
        subtitle3 = re.sub('&ldquo;|&rdquo;|&quot;', '"', subtitle2)

        re_compile = '<img(.*?)>|&#13;|\u00a0|<small(.*?)>|</small>|<em(.*?)>|</em>|<b(.*?)>|</b>|<a(.*?)>|</a>|<span(.*?)>|</span>|<strong(.*?)>|</strong>|<div(.*?)>|</div>|<script>(.*?)</script>'
        subtitle4 = re.sub(re_compile, '', subtitle3)
        return self.clean_space(subtitle4.strip())
        # return re.sub(r"\s+", '', subtitle4.strip())

    def clean_space(self, text):
        """"
        处理中文多余的空格
        """
        match_regex = re.compile(u'[\u4e00-\u9fa5。\.,，:：《》、\(\)（）]{1} +')
        should_replace_list = match_regex.findall(text)
        for i in should_replace_list:
            if i == u' ':
                continue
            new_i = i.strip()
            text = text.replace(i, new_i)
        return text

    def process_item(self, item, spider):
        # 如果有subtitle 并对subtitle进行特殊符号的替换
        if item.get(settings.item_subtitle):
            item[settings.item_subtitle] = self.clear_spaces(item[settings.item_subtitle])
        if item.get(settings.item_title):
            item[settings.item_title] = self.clear_spaces(item[settings.item_title])
        if item.get(settings.item_other):
            item[settings.item_other] = self.clear_spaces(item.get(settings.item_other))
        # 如果app的类型为news和job或图片字段为NULL不进行图片判重处理
        # if item[settings.item_type] == 'jobs' or item[settings.item_image_link] == 'NULL' or item[
        #     settings.item_type] == 'news':
        #     pass
        # else:
        #     img = item[settings.item_image_link]
        #     if img not in spider.img_list:
        #         if img:
        #             spider.img_list.append(img)
        #         else:
        #             item[settings.item_image_link] = ''
        #     else:
        #         raise DropItem("图片重复")
        return item


    # 将获得的数据放入爬虫集群的本地redis


class NewPipeline(object):

    @classmethod
    def from_crawler(cls, crawler):
        MYSQL_PORT = settings.MYSQL_PORT
        MYSQL_PASWORD = settings.MYSQL_PASWORD
        MYSQL_HOST = settings.MYSQL_HOST
        MYSQL_DB = settings.MYSQL_DB
        MYSQL_USER = settings.MYSQL_USER

        return cls(MYSQL_PORT, MYSQL_HOST, MYSQL_USER, MYSQL_PASWORD, MYSQL_DB)

    # 初始化参数
    def __init__(self, MYSQL_PORT, MYSQL_HOST, MYSQL_USER,MYSQL_PASWORD, MYSQL_DB):

        params = dict(
            host=MYSQL_HOST,
            user=MYSQL_USER,
            password=MYSQL_PASWORD,
            db=MYSQL_DB,
            charset='utf8',  # 不能用utf-8
            cursorclass=pymysql.cursors.DictCursor
        )
        # 使用Twisted中的adbapi获取数据库连接池对象
        self.dbpool = adbapi.ConnectionPool('pymysql', **params)
        print(444444444)

        pass

    # 建立Redis连接
    def open_spider(self, spider):
        """
        爬虫启动时，启动
        :param spider:
        :return:
        """
        pass

    def close_spider(self, spider):
        self.dbpool.close()

    def process_item(self, item, spider):
        # 使用数据库连接池对象进行数据库操作,自动传递cursor对象到第一个参数
        query = self.dbpool.runInteraction(self.do_insert, item)
        # 设置出错时的回调方法,自动传递出错消息对象failure到第一个参数
        query.addErrback(self.on_error, spider, item)
        return item

    def do_insert(self, cursor, item):
        table_name = 'spider_quick'
        sql = 'REPLACE INTO {} ({}) VALUES  {};'.format(table_name, ','.join(list(dict(item).keys())),
                                                               tuple(dict(item).values()))
        cursor.execute(sql.replace("'None'", "NULL").replace("None", "NULL"))

    def on_error(self, failure, spider, item):
            spider.logger.error(failure)
            ErrorSignal.main(spider.name, str(failure), item.get('query'), error=None, vers =item.get('vers'))



