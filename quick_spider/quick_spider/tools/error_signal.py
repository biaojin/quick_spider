
import json
import time
import redis
import socket
import urllib.request
from quick_spider.tools.spider_tools import return_error


class ErrorSignal(object):
    """
    :这是一个利用钉钉机器人进行钉钉群发送爬虫程序报错的类
    """

    @staticmethod
    def main(name, text, query=None, error=None, vers=None):
        # 按照钉钉给的数据格式设计请求内容  链接https://open-doc.dingtalk.com/docs/doc.htm?spm=a219a.7629140.0.0.p7hJKp&tree
        # Id=257&articleId=105735&docType=1
        content = name+"爬虫出现错误+%s\n%s"%(query, str(text))
        # todo 在参数加一个version
        if error is not None:
            # error_dict = dict()
            hostname = socket.gethostname()
            reason = str(error.__traceback__.tb_frame.f_globals['__file__'])+':'+str(error.__traceback__.tb_lineno)+':'+str(error)
            error_dict = {
                'app': name,
                'query': query,
                'code': '0',
                'vers': vers,
                'crawl_time': time.strftime('%Y-%m-%d', time.localtime()),
                'reason': reason,
                'hostName': hostname
            }
            return_error(name, json.dumps(error_dict, ensure_ascii=False))
        my_data = {
            "msgtype": "markdown",
            "markdown": {"title": "爬虫程序错误报警",
                         "text": content
                         },
            "at": {

                "isAtAll": True
            }
        }

        header = {
            "Content-Type": "application/json",
            "Charset": "UTF-8"
        }
        sendData = json.dumps(my_data)  # 将字典类型数据转化为json格式
        sendDatas = sendData.encode("utf-8")  # python3的Request要求data为byte类型
        my_url = "https://oapi.dingtalk.com/robot/send?" \
                 "access_token=bbf6ab78bc5dbfeab3e6c3813df2ffc87d2ee56854d6386abdc457b5ed30cccf"
        # 发送请求
        # time.sleep(2)
        request = urllib.request.Request(url=my_url, data=sendDatas, headers=header)
        # 将请求发回的数据构建成为文件格式
        opener = urllib.request.urlopen(request)
        # 7、打印返回的结果
        print(opener.read())


# if __name__ == "__main__":
#    ErrorSignal.main('aaa', "爬虫出现错误", 'gg', e)