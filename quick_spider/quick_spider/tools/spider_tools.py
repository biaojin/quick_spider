# -*- coding:utf-8 -*-
import redis
import re
from quick_spider import settings
from quick_spider.configs.AppInfo import app_info_dict
import time
import json
import socket
import datetime


from dateutil.relativedelta import relativedelta
pool = redis.ConnectionPool(host=settings.REDIS_HOST, port=int(settings.REDIS_PORT), decode_responses=True, db=4, password=settings.REDIS_PARAMS.get('password'))
r = redis.Redis(connection_pool=pool)
pool_1 = redis.ConnectionPool(host=settings.push_redis_host, port=int(settings.push_redis_port),
                        decode_responses=True)
pydis = redis.Redis(connection_pool=pool_1)

def has_con(cls, query, con, code, keyword=''):
    pass


def check_in_desktop(text, name):
    # print(os.getcwd())
    # if os.getcwd().startswith('/Users/apple/Desktop'):
    #     if isinstance(text, dict):
    #         text = json.dumps(text)
    #     with open('/Users/apple/Desktop/' + name + '.txt', 'w+') as f:
    #         f.write(text)
    # else:
    #     with open('/home/qury/qury-crawler/P_QurySpider/P_QurySpider/'+name+'.txt', 'w+') as f:
    #         f.write(text)
    pass


def write_erro(e, name):
    pass


def make_item(app_package_id, item, query, vers):
    hostname = socket.gethostname()
    item[settings.app_token] = app_package_id.replace('_tv', '').replace('_movie', '').replace('_brand', '').replace('_phone', '').replace('_computer', '').replace('_flat', '')
    item[settings.query] = query
    item[settings.country] = app_info_dict[app_package_id][settings.country]
    item[settings.item_category] = app_info_dict[app_package_id][settings.item_category]
    item[settings.item_crawl_time] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    item[settings.item_host_name] = hostname
    item[settings.vers] = vers


# 存放有多少个query需要请求
def return_all_query(app_package_id, data):
    pydis = redis.Redis(host=settings.REDIS_HOST, port=int(settings.REDIS_PORT),
                        decode_responses=True, db=4,
                        password=settings.REDIS_PARAMS.get('password'))
    pydis.incr('{}ALL_QUERY_NUM'.format(json.loads(data).get('vers')), amount=1)
    # pydis.sadd(app_package_id + ':all_query', query)
    pass


# 存放请求失败返回的query
def return_error(app_package_id, query):
    pydis = redis.Redis(host=settings.push_redis_host, port=int(settings.push_redis_port),
                        decode_responses=True)
    data = json.loads(query)
    hostname = socket.gethostname()
    if not data.get('app', ''):data.update({
        'app': app_package_id
    })
    data.update({
        'hostName': hostname
    })
    pydis.rpush('error_items', json.dumps(data))
# 带着token请求dailymoth获取auth


def write_html_source(app_package_id, source):
    with open(settings.SOURCE_PATH + app_package_id + '.txt', 'w+') as f:
        f.write(source)

# 存放没有数据返回的query
def return_no_result(app_package_id, query, vers):
    # pydis = redis.Redis(host=settings.push_redis_host, port=int(settings.push_redis_port),
    #                     decode_responses=True)

    hostname = socket.gethostname()

    data = {
        'app': app_package_id,
        'query': query,
        'vers': vers,
        'crawl_time': time.strftime('%Y-%m-%d', time.localtime()),
        'hostName': hostname
    }

    pydis.rpush('noresult_items', json.dumps(data))

# 将失败的query添加到任务队列
def add_error_task(app_package_id, query):
    pydis = redis.Redis(host=settings.REDIS_HOST, port=int(settings.REDIS_PORT),
                        decode_responses=True, db=settings.REDIS_PARAMS.get('db'),
                        password=settings.REDIS_PARAMS.get('password'))
    pydis.delete(app_package_id+':dupefilter')
    pydis.rpush(app_package_id+':start_urls', query)


# 解析带有天，周，月的时间
def date_days(data):
    num = ''.join(re.findall(r'\d+', data))
    if num:
        if 'day' in data and 'days' in data:
            time_data = int(num)
        elif 'weeks' in data or 'week' in data:
            time_data = int(num) * 7
        elif 'months' in data or 'month' in data:
            time_data = int(num) * 30
        else:
            time_data = int(1)
        item_time = (datetime.datetime.now() + datetime.timedelta(days=-int(time_data))).strftime('%Y-%m-%d')
    else:
        item_time = None
    return item_time


# 解析带有小时，分钟的时间
def date_hour(data):
    num = ''.join(re.findall(r'\d+', data))
    if num:
        if 'mins' in data or 'minutes' in data:
            time_data = int(num)
            item_time = (datetime.datetime.now() + datetime.timedelta(minutes=-int(time_data))).strftime('%Y-%m-%d')
        elif 'hours' in data:
            time_data = int(num)
            item_time = (datetime.datetime.now() + datetime.timedelta(hours=-int(time_data))).strftime('%Y-%m-%d')
        else:
            time_data = int(1)
            item_time = (datetime.datetime.now() + datetime.timedelta(seconds=-int(time_data))).strftime('%Y-%m-%d')
    else:
        item_time = None

    return item_time


def date_year(data):
    num = ''.join(re.findall(r'\d+', data))
    print(num)
    if num:
            time_year = int(num)
            from datetime import datetime
            item_time = (datetime.now() - relativedelta(years=time_year)).strftime('%Y-%m-%d')
    else:
        item_time = None

    return item_time


# 把时间转换为时间戳
def turn_Timestamp(num):
    matches_list = re.findall('\d{4}[-]\d{1,2}[-]\d{1,2}', str(num))
    if matches_list:
        itemTimestamp = int(time.mktime(time.strptime(matches_list[0], "%Y-%m-%d"))) * 1000
        return itemTimestamp
    else:
        return None


# 解析时间的函数
def extraction_time(data, appname=None):
    data = str(data)
    # num = []
    # time_data = 0
    # date_reg_exp = re.compile('\d{2}[-/]\d{2}[-/]\d{4}')
    # matches_list = date_reg_exp.findall(data)
    # if matches_list:
    #     matches = matches_list[0].split('/')
    #     return matches[2]+"-"+matches[0]+"-"+matches[1]

    list_re = ['\d{4}[-/]\d{1,2}[-/]\d{1,2}', '\w+ \d{1,2} \d{4}', '\d{1,2}[-/]\d{1,2}[-/]\d{4}', '\w+. \d{1,2}, \d{4}',
               '\d{1,2}[ ]\w+[ ]\d{4}', '\w+ \d{1,2},\s{1,2}\d{4}', '\d{1,2} \w+, \d{4}', '\d{1,2}, \w+ \d{4}', '[A-Za-z]+ \d{1,2}', '\d{1,2}-\w+-\d{4}']
    month = {'January': '01', 'Jan': '01', 'February': '02', 'Feb': '02', 'March': '03', 'Mar': '03', 'April': '04',
             'Apr': '04', 'May': '05', 'June': '06', 'Jun': '06', 'July': '07', 'Jul': '07', 'August': '08',
             'Aug': '08',
             'September': '09', 'Sep': '09', 'October': '10', 'Oct': '10', 'November': '11', 'Nov': '11',
             "December": '12',
             "Dec": '12', 'Sept': '09'}
    if appname == 'com.thetelegraph_in':
        date_reg_exp = re.compile('\d{2}[.]\d{2}[.]\d{2}')
        matches_list = date_reg_exp.findall(data)
        if matches_list:
            matches = matches_list[0].split('.')
            matches = '20{}-{}-{}'.format(matches[2], matches[1], matches[0])
            return matches

    for i in list_re:
        date_reg_exp = re.compile(i)
        matches_list = date_reg_exp.findall(data)
        if matches_list and i == '\w+ \d{1,2},\s{1,2}\d{4}':
            matches = matches_list[0].replace(',', '').split(' ')
            if len(matches) == 3:
                matches = '{}-{}-{}'.format(matches[2], month[matches[0]], matches[1])
                return matches
            elif len(matches) == 4:
                matches = '{}-{}-{}'.format(matches[3], month[matches[0]], matches[1])
                return matches

        elif matches_list and i == '\d{1,2}[ ]\w+[ ]\d{4}':
            matches = matches_list[0].split(' ')
            matches = '{}-{}-{}'.format(matches[2], month[matches[1]], matches[0])
            return matches

        elif matches_list and i == '\d{4}[-/]\d{1,2}[-/]\d{1,2}':
            if '/' in matches_list[0]:
                matches = matches_list[0].split('/')
                matches = '{}-{}-{}'.format(matches[0], matches[1], matches[2])
                return matches
            else:
                return matches_list[0]

        elif matches_list and i == '\w+ \d{1,2} \d{4}':
            matches = matches_list[0].split(' ')
            matches = '{}-{}-{}'.format(matches[2], month[matches[0]], matches[1])
            return matches

        elif matches_list and i == '\d{1,2}[-/]\d{1,2}[-/]\d{4}':
            if '/' in matches_list[0]:
                matches = matches_list[0].split('/')
                if appname == 'com.mediacorp.sg.beritamediacorp_sg':
                    matches = '{}-{}-{}'.format(matches[2], matches[1], matches[0])
                else:
                    matches = '{}-{}-{}'.format(matches[2], matches[0], matches[1])
                return matches

        elif matches_list and i == '\w+. \d{1,2}, \d{4}':
            matches = matches_list[0].replace(',', '').replace('.', '').split(' ')
            matches = '{}-{}-{}'.format(matches[2], month[matches[0]], matches[1])
            return matches

        elif matches_list and i == '\d{1,2} \w+, \d{4}' or matches_list and i == '\d{1,2}, \w+ \d{4}' or matches_list and i == '\d{1,2}-\w+-\d{4}':
            if i == '\d{1,2}-\w+-\d{4}':
                matches = data.split('-')
            else:
                matches = matches_list[0].replace(',', '').split(' ')
            matches = '{}-{}-{}'.format(matches[2], month[matches[1]], matches[0])
            return matches

        elif matches_list and i == '[A-Za-z]+ \d{1,2}':
            matches = matches_list[0].replace(',', '').split(' ')
            matches = '{}-{}-{}'.format(datetime.datetime.now().year, month[matches[0]], matches[1])
            return matches

        else:
            if 'Monday' in data or 'Tuesday' in data or 'Wednesday' in data or 'Thursday' in data or 'Friday' in data or 'Saturday' in data or 'Sunday' in data:
                pass
            elif 'day' in data or 'weeks' in data or 'months' in data or 'days' in data or 'month' in data or 'week' in data or 'Days' in data or 'Months' in data:
                return date_days(data)
            elif 'mins' in data or 'minutes' in data or 'hours' in data or 'hour' in data or 'H' in data or 'M' in data or '小时':
                return date_hour(data)
            elif 'year' in data:
                return date_year(data)


def get_date_list(Timestamp=None, year=3):
    """
    获取时间列表
    :param begin_date:
    :return:
    """
    if Timestamp and len(str(Timestamp)) == 13:
        data = datetime.datetime.now() - relativedelta(years=int(year))
        end_date = int(time.mktime(data.timetuple()))*1000
        if int(Timestamp) >= end_date:
            return True
        else:
            return False
    else:
        return False
