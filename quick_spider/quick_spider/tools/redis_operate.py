# -*- coding: utf-8 -*-
# @Author  : 
# @File    : redis_operate.py
# @Software: PyCharm
# @description : XXX

import redis
import json
import sys
if sys.platform == 'win32':
    REDIS_HOST = '35.199.186.52'
    REDIS_PORT = 6666
    REDIS_PARAMS = {'password': 'Qury666!@#', 'db': 3}
elif sys.platform == 'darwin':
    REDIS_HOST = '35.199.186.52 '
    REDIS_PORT = 6666
    REDIS_PARAMS = {'password': 'Qury666!@#', 'db': 0}
else:
    REDIS_HOST = '10.138.0.11'
    REDIS_PORT = 6666
    REDIS_PARAMS = {'password': 'Qury666!@#', 'db': 0}
redis_server = REDIS_HOST
redis_port = REDIS_PORT
redis_password = REDIS_PARAMS['password']
redis_db = 2


class RedisTaskQueueOperate(object):
    """
        redis queue operate
    """

    def __init__(self):
        """
            初始化 redis 队列类型 和 redis db
        """

        self.__redis_pool = redis.ConnectionPool(
            host=redis_server,
            port=redis_port,
            password=redis_password,
            db=redis_db,
            decode_responses=True
        )
        self.__redis_db = redis.StrictRedis(connection_pool=self.__redis_pool)
        self.__redis_pipeline = self.__redis_db.pipeline()
        self.__r_pipe = self.__redis_pipeline
        pass

    def __del__(self):
        """
            析构，释放 redis 连接
        :return:
        """
        # ConnectionPool.disconnect() does in fact close all the connections opened from that connection pool.
        # It does not prevent new connections from being opened however
        # r.connection_pool.disconnect()
        self.__redis_pool.disconnect()

    def redis_pipeline_execute(self):
        self.__r_pipe.execute()
        # self.__redis_pool.disconnect()

    def add_task(self, key, dict_data, score=1000):
        """
            向 redis 添加任务, 一次添加一个
        :param key:       redis_key
        :param dict_data: Python dict
        :param score:     优先级。默认是1000
        :return:          0: 成功。 1: 失败。-1: 函数没有执行
        """

        ret_val = -1
        try:
            self.__r_pipe.zadd(key, {json.dumps(dict_data, ensure_ascii=False): score})

            # self.__r_pipe.zadd(key, dict_data, score)
            ret_val = 0
            # print(4444)
        except BaseException as e:
            print(e, '添加失败')
            ret_val = 1
        # self.mongo_close()
        return ret_val

    def write_log(self, spider_name, log_info):
        self.__r_pipe.lpush('log:{0}'.format(spider_name), log_info).execute()
        pass

    def get_url_tok(self, app_package_id):
        return self.__r_pipe.lindex(app_package_id, 0).execute()

    # 获取booking 城市名称的代号
    def get_booking_city_codename(self, query):
        return self.__r_pipe.get('com.booking:' + query).execute()

    # 将城市名称的代号存入redis中
    def set_booking_city_codename(self, query, codename):
        self.__r_pipe.set('com.booking:' + query, codename).execute()

    # 获取dailymotion的用户认证信息
    def get_dailymotion_auth(self,):
        return self.__r_pipe.get('dilymotion_auth').execute()[0]

    # 删除不能使用的dailymotion的用户认证信息
    def del_dailymotion_auth(self, auth):
        return self.__r_pipe.delect('dilymotion_auth', auth).execute()


rq = RedisTaskQueueOperate()
get_dailymotion_auth = rq.get_dailymotion_auth
set_booking_city_codename = rq.set_booking_city_codename
get_booking_city_codename = rq.get_booking_city_codename
get_url_tok = rq.get_url_tok
add_task = rq.add_task
redis_pipeline_execute = rq.redis_pipeline_execute
write_log = rq.write_log
del_dailymotion_auth = rq.del_dailymotion_auth





