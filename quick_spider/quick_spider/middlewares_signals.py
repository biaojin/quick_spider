import logging
import json
from scrapy import signals
import datetime
from quick_spider.tools.error_signal import ErrorSignal
import time
import socket
from threading import Timer
import redis
from quick_spider.settings import REDIS_HOST, REDIS_PORT, REDIS_PARAMS

logger = logging.getLogger(__name__)


class SpiderStatLogging:

    def __init__(self, crawler, interval):
        self.exit_code = False
        self.interval = interval
        self.crawler = crawler
        self.__redis_pool = redis.ConnectionPool(
            host=REDIS_HOST,
            port=REDIS_PORT,
            password=REDIS_PARAMS.get('password'),
            db=REDIS_PARAMS.get('db'),
            decode_responses=True
        )
        self.__redis_db = redis.StrictRedis(connection_pool=self.__redis_pool)
        self.__redis_pipeline = self.__redis_db.pipeline()

    @classmethod
    def from_crawler(cls, crawler):
        interval = crawler.settings.get('INTERVAL', 1)
        print(interval)
        ext = cls(crawler, interval)
        crawler.signals.connect(ext.engine_started, signal=signals.engine_started)
        crawler.signals.connect(ext.engine_stopped, signal=signals.engine_stopped)
        crawler.signals.connect(ext.spider_closed, signal=signals.spider_closed)
        crawler.signals.connect(ext.spider_opened, signal=signals.spider_opened)
        return ext

    def spider_closed(self, spider, reason):
        pass

    def spider_opened(self, spider):
        pass

    def engine_started(self):
        pass

    def engine_stopped(self):
        self.exit_code = True
        Timer(self.interval, self.handle_stat).start()

    # 对爬取数据统计
    def handle_stat(self):
        stats = self.crawler.stats.get_stats()
        print(1111, stats)
        d = {
            'response': stats.get('downloader/response_count', 0),
            'item': stats.get('item_scraped_count', 0),
            'spider_name': self.crawler.spider.name,
            'vers': stats.get('vers', 0)
        }
        if d.get('item') == 0 and d.get('vers') != 0:
            server_name = socket.gethostname()
            ErrorSignal().main(self.crawler.spider.name, '%s版本号%s热门内容爬取为空' % (server_name, d.get('vers')))
        self.insert2redis(d)
        print(33333333333333333333333333333, d)

        if not self.exit_code:
            Timer(self.interval, self.handle_stat).start()

    def insert2redis(self, value):
        if value.get('vers') == 0:
            pass
        else:
            key = 'quick_spider_NUM:' + str(value.get('vers'))
            self.__redis_pipeline.rpush(key, json.dumps(value)).execute()
            self.__redis_pipeline.expire(key, 259200).execute()


# 等待时间，自动结束脚本
class RedisSpiderClosedExensions(object):

    def __init__(self, idle_number, crawler):
        self.crawler = crawler
        self.idle_number = idle_number
        self.idle_list = []
        self.idle_count = 0

    @classmethod
    def from_crawler(cls, crawler):
        # IDLE_NUMBER目前被被设定为等待时间，IDLE一个时间片5秒，所以setting.py中设置的时间除以5就是时间片的数量
        idle_number = crawler.settings.getint('IDLE_NUMBER', 80)
        ext = cls(idle_number, crawler)
        crawler.signals.connect(ext.spider_idle, signal=signals.spider_idle)

        return ext

    def spider_idle(self, spider):
        self.idle_count += 1
        self.idle_list.append(time.time())
        idle_list_len = len(self.idle_list)
        if idle_list_len > 2 and self.idle_list[-1] - self.idle_list[-2] > 6:
            self.idle_list = [self.idle_list[-1]]
            # logging.info('\n continued idle number exceed {} Times'
            #         '\n meet the idle shutdown conditions, will close the reptile operation'
            #         '\n idle start time: {},  close spider time: {}'.format(self.idle_number,
            #                                                               self.idle_list[0], self.idle_list[0]))
        elif idle_list_len > self.idle_number:
            self.crawler.engine.close_spider(spider, 'closespider_pagecount')

