# -*- coding:utf-8 -*-
import sys
import os
import subprocess
import re
import time

host = str(sys.argv[1])
start = int(sys.argv[2])
end = int(sys.argv[3])

host_name = ''
if '6666' == host:
    host_name = 'py-scrapy'
elif '7777' == host:
    host_name = 'daily-realtime-cralwer'
elif '8888' == host:
    host_name = 'daily-topquery-cralwer'

def scp_task():
    for i in range(start, end + 1):
        idx = str(i).zfill(2)
        crawler_name = "qury@" + host_name + "-" + idx
        rm_cmd = 'ssh ' + crawler_name + ' " rm -rf /home/qury/quick_spider/"'
        scp_cmd = 'scp -r /home/qury/quick_spider/quick_spider/ ' + crawler_name + ':/home/qury/'

        # 复制data-platform
        dp_rm_cmd = 'ssh ' + crawler_name + ' " rm -rf /home/qury/data-platform-new/"'
        dp_scp_cmd = 'scp -r /home/qury/data-platform-new/ ' + crawler_name + ':/home/qury/'
        
        linshi_scp_cmd = 'scp 6666.conf ' + crawler_name + ':/home/qury/redis-5.0.8/src'

        ch_en1 = 'ssh ' + crawler_name + ' "sudo mv /usr/bin/python3 /usr/bin/python_bak"'
        ch_en2 = 'ssh ' + crawler_name + ' "sudo ln -s /usr/local/python3/bin/python3.8 /usr/bin/python3"'
        os.system(rm_cmd)
        os.system(scp_cmd)
        #os.system(linshi_scp_cmd)
        print(crawler_name + ' update succeed !')


def shut_crawler():
    result = subprocess.getoutput('ps -ef | grep python3')
    param = result.split('\n')
    for line in param:
        if 'SlaverRun.py' in line or 'scrapy' in line:
            pid = re.sub(' +', ' ', line.rstrip()).split(' ')[1]
            os.system(' kill -9 ' + str(pid))

def shut_process():
    result = subprocess.getoutput('ps -ef | grep python3')
    param = result.split('\n')
    for line in param:
        if 'process_item.py' in line:
            pid = re.sub(' +', ' ', line.rstrip()).split(' ')[1]
            os.system(' kill -9 ' + str(pid))



def shut_pro():
    result = subprocess.getoutput('ps -ef | grep python3')
    param = result.split('\n')
    for line in param:
        if 'proc_item.py' in line:
            pid = re.sub(' +', ' ', line.rstrip()).split(' ')[1]
            os.system(' kill -9 ' + str(pid))


def shut_collect():
    result = subprocess.getoutput('ps -ef | grep python3')
    param = result.split('\n')
    for line in param:
        if 'collect_items' in line or 'collect_data' in line:
            pid = re.sub(' +', ' ', line.rstrip()).split(' ')[1]
            os.system(' kill -9 ' + str(pid))



if __name__ == '__main__':
    opt = sys.argv[4]
    comm = sys.argv[5]

    # 复制项目
    if opt == 'copy':
        scp_task()

    # 关闭爬虫进程
    if opt == 'shut_crawler':
        for i in range(start, end + 1):
            idx = str(i).zfill(2)
            crawler_name = "qury@" + host_name + "-" + idx
            print('------------{}----------'.format(crawler_name))
            task = 'ssh ' + crawler_name + ' " sudo python3 /home/qury/data-platform-new/script/scp_data.py 6666 1 1 shut_crawler_slave 1"'
            os.system(task)

    # 关闭数据处理进程
    # if opt == 'shut_process':
    #     for i in range(start, end + 1):
    #         idx = str(i).zfill(2)
    #         crawler_name = "qury@" + host_name + "-" + idx
    #         print('------------{}----------'.format(crawler_name))
    #         task = 'ssh ' + crawler_name + ' " sudo python3 /home/qury/data-platform-new/script/scp_data.py 6666 1 1 shut_process_slave 1"'
    #         os.system(task)

    if opt == 'shut_pro':
        for i in range(start, end + 1):
            idx = str(i).zfill(2)
            crawler_name = "qury@" + host_name + "-" + idx
            print('------------{}----------'.format(crawler_name))
            task = 'ssh ' + crawler_name + ' " sudo python3 /home/qury/data-platform-new/script/scp_data.py 6666 1 1 shut_pro_slave 1"'
            os.system(task)
    # 这个可以通过scrapy 01 进行控制某台服务器的有collect的进程
    if opt == 'shut_collect':
        for i in range(start, end + 1):
            idx = str(i).zfill(2)
            crawler_name = "qury@" + host_name + "-" + idx
            print('------------{}----------'.format(crawler_name))
            task = 'ssh ' + crawler_name + ' " sudo python3 /home/qury/data-platform-new/script/scp_data.py 6666 1 1 shut_collect_slave 1"'
            os.system(task)

    # ps -ef
    if opt == 'ps':
        for i in range(start, end + 1):
            idx = str(i).zfill(2)
            crawler_name = "qury@" + host_name + "-" + idx
            print('------------{}----------'.format(crawler_name))
            task = 'ssh ' + crawler_name + ' " ps -ef | grep python3"'
            os.system(task)

    # 子程序执行函数
    if opt == 'shut_process_slave':
        shut_process()

    # 子程序执行函数  停止运行爬虫脚本
    if opt == 'shut_crawler_slave':
        shut_crawler()

    if opt == 'shut_pro_slave': # 停止运行图片处理的脚本
        shut_pro()

    if opt == 'shut_collect_slave':
        shut_collect()

    if opt == 'run':
        for i in range(start, end + 1):
            idx = str(i).zfill(2)
            crawler_name = "qury@" + host_name + "-" + idx
            print('------------{}----------'.format(crawler_name))
            task = 'ssh ' + crawler_name + ' {}'.format(comm)
            os.system(task)

    if opt == 'run_collect':
        for i in range(start, end + 1):
            idx = str(i).zfill(2)
            crawler_name = "qury@" + host_name + "-" + idx
            print('------------{}----------'.format(crawler_name))
            # task1 = 'ssh ' + crawler_name + ' " nohup python3 /home/qury/data-platform-new/crawl/collect_data.py > /home/qury/data-platform-new/crawl/Cdata.log &"'
            # task2 = 'ssh ' + crawler_name + ' " nohup python3 /home/qury/data-platform-new/crawl/collect_items.py > /home/qury/data-platform-new/crawl/Citems.log &"'
            task1 = 'ssh ' + crawler_name + ' " cd /home/qury/data-platform-new/crawl/ ; nohup python3 collect_data.py > /home/qury/data-platform-new/crawl/Cdata.log &"'
            task2 = 'ssh ' + crawler_name + ' " cd /home/qury/data-platform-new/crawl/ ; nohup python3 collect_items.py > /home/qury/data-platform-new/crawl/Citems.log &"'
            os.system(task1)
            os.system(task2)

    if opt == 'run_pro':
        for i in range(start, end + 1):
            idx = str(i).zfill(2)
            crawler_name = "qury@" + host_name + "-" + idx
            print('------------{}----------'.format(crawler_name))
            # task1 = 'ssh ' + crawler_name + ' " nohup python3 /home/qury/data-platform-new/utils/proc_item.py > /home/qury/data-platform-new/utils/proc_item.log &"'
            task = 'ssh ' + crawler_name + ' " cd /home/qury/data-platform-new/utils/ ; nohup python3 proc_item.py > /home/qury/data-platform-new/utils/proc_item.log &"'
            os.system(task)
            os.system(task)
            os.system(task)

