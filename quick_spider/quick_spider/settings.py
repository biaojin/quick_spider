# 根据机器名调整配置
import socket
hostname = int(socket.gethostname().split('-')[-1]) if 'py' in socket.gethostname() else 999


# CRAWLERA_DEFAULT_HEADERS=
BOT_NAME = 'quick_spider'

SPIDER_MODULES = ['quick_spider.spiders']
NEWSPIDER_MODULE = 'quick_spider.spiders'

FILES_STORE = '/Users/apple/Desktop/imgs'

# CRAWLERA_APIKEY_IN = '101c78709b7145bd8156b2f33521e699'
# CRAWLERA_APIKEY_PH = '101c78709b7145bd8156b2f33521e699'
# CRAWLERA_APIKEY_SG = '100f52df2a394d07b5043d0c056a9ef0'
CRAWLERA_APIKEY_ALL = 'e009fc666edb4f9d8ad928b742ab71c0'
CRAWLERA_ENABLED = True
AUTOTHROTTLE_ENABLED = False

# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html


ITEM_PIPELINES = {
    # 'scrapy_redis.pipelines.RedisPipeline': 300,
   'quick_spider.pipelines.NewPipeline': 299,
    'quick_spider.pipelines.HotSpiderPipeline': 298,
    # 'P_QurySpider.pipelines.DownLoadImgPipeline': 298,
}

IMAGES_URLS_FIELD = 'image_urls'
SELENIUM_TIMEOUT = 100

SCROLLTOP = 10000

MYEXT_ENABLED=True
# scrapy utils
SCHEDULER = "scrapy_redis.scheduler.Scheduler"
DUPEFILTER_CLASS = "scrapy_redis.dupefilter.RFPDupeFilter"
#
EXTENSIONS = {
    'quick_spider.middlewares_signals.RedisSpiderClosedExensions': 450
}
IDLE_NUMBER = 80  # 配置空闲持续时间单位为 100个 ，
MYSQL_PORT = 3306
MYSQL_PASWORD ='tBqkC0$Lrv$cEqOiK*'
MYSQL_HOST ='34.126.174.18'
MYSQL_DB ='search_chinese'
MYSQL_USER ='u_search_item'
import sys
if sys.platform == 'win32':
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6666
    REDIS_PARAMS = {'password': '', 'db': 0}
    IP = 'spider.qury.me'
    DOWNLOADER_MIDDLEWARES = {
        'quick_spider.middlewares.ErrorSpiderMiddleware': 300,
        # 'quick_spider.middlewares.TwitterDownloaderMiddleware': 609,
      # 'quick_spider.middlewares.CrawleraMiddleware': 610,
        #'quick_spider.middlewares.ScraperProxiesMiddleware': 610,
        # 'scrapy_crawlera.CrawleraMiddleware': 610,

    }
elif sys.platform == 'darwin':
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6666
    REDIS_PARAMS = {'password': '', 'db': 0}
    IP = 'spider.qury.me'
    DOWNLOADER_MIDDLEWARES = {
        'quick_spider.middlewares.ErrorSpiderMiddleware': 300,
        'quick_spider.middlewares.TwitterDownloaderMiddleware': 609,
        # 'P_QurySpider.middlewares.CrawleraMiddleware': 610,
        # 'P_QurySpider.middlewares.ScraperProxiesMiddleware': 610,
        # 'scrapy_crawlera.CrawleraMiddleware': 610,

    }
else:

    # py-scrapy-01 每周爬虫
    REDIS_HOST = '10.138.0.11'
    REDIS_PORT = 6666
    IP = '10.138.0.32:5000'
    REDIS_PARAMS = {'password': 'Qury666!@#', 'db': 0}

    DOWNLOADER_MIDDLEWARES = {
        'quick_spider.middlewares.ErrorSpiderMiddleware': 300,
        # 'quick_spider.middlewares.TwitterDownloaderMiddleware': 609,
        'quick_spider.middlewares.CrawleraMiddleware': 610,
        # 'quick_spider.middlewares.ScraperProxiesMiddleware': 610,
        # 'scrapy_crawlera.CrawleraMiddleware': 610,

    }
SPIDER_MIDDLEWARES = {
'scrapy.spidermiddlewares.urllength.UrlLengthMiddleware': None
}

import sys
if sys.platform == 'win32':
    push_redis_host = '127.0.0.1'
    push_redis_port = 6666
    push_redis_param = {'password': '', 'db': 0}

elif sys.platform == 'darwin':
    push_redis_host = '127.0.0.1'
    push_redis_port = 6666
    push_redis_param = {'password': '', 'db': 0}
else:
    # 新增：根据机器名调整数据库
    if hostname in range(10, 20):
        push_redis_host = '127.0.0.1'
        push_redis_port = 6666
        push_redis_param = {'password': '', 'db': 0}
    else:
        push_redis_host = '127.0.0.1'
        push_redis_port = 6666
        push_redis_param = {'password': '', 'db': 0}

# py-scrapy-01 每周爬虫

# dynamic-crawler-master 实时爬虫和每日top query爬虫
# REDIS_HOST = '34.83.54.234'

# 每周爬虫6666，实时爬虫7777，每日top query爬虫8888

SCHEDULER_QUEUE_CLASS = 'scrapy_redis.queue.PriorityQueue'

SCHEDULER_PERSIST = True

AUTOTHROTTLE_START_DELAY = 0.5  # 初始下载延迟
DOWNLOAD_DELAY = 0.5  # 每次请求间隔时间
COOKIES_ENABLED = True
FEED_EXPORT_ENCODING = 'utf-8'
CONCURRENT_REQUESTS = 5


rank = 'rank'
page = 'page'
app_token = 'appToken'
query = 'query'
country = 'country'
vers = 'vers'
item_title = 'itemTitle'
item_detail_link = 'itemDetailLink'
item_image_link = 'itemImageLink'
item_type = 'itemType'
item_rank = 'itemRank'
item_subtitle = 'subtitle'
item_category = 'category'
item_crawl_time = 'crawl_time'
item_name = "itemName"
item_adder = "itemAdder"
item_price = 'itemPrice'
item_time = "itemTime"
item_other = "itemOther"
item_Timestamp = 'itemTimestamp'
item_list = 'itemList'
item_host_name = 'hostName'
item_AliasCN = 'AliasCN'
item_DiseaseTagName = 'DiseaseTagName'
item_RelateBody = 'RelateBody'
item_DiseaseList = 'DiseaseList'
item_Symptom = 'Symptom'
item_Cause = 'Cause'
item_XyTreated = 'XyTreated'
item_Onelabel = 'Onelabel'
item_appType = 'appType'
item_duration = 'itemDuration'
item_colour = 'colour'  # 默认颜色选项
item_Comment_tags = 'Commenttags'  # 评价标签词
item_Comment = 'Comment'  # 评论
item_Praise = 'Praise'  # 好评
item_Twolabel = 'Twolabel'
item_class = 'itemClass'
server_type = 'serverType'
DNSCACHE_ENABLED = False
